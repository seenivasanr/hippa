const user = require('../controllers/userCtrl');
const dashboard = require('../controllers/dashboardCtrl.js');
const common = require('../common/common');

module.exports = function(app, passport) {
    app.get('/login', function(req, res) {
        res.render("pages/auth/login");
    })

    app.get('/forgotpassword', (req, res) => {
        res.render('pages/auth/forgot_password');
    });

    app.get('/reset', (req, res) => {
        res.render('pages/auth/reset_password', {data: req.params});
    });

    app.get('/logout', function (req, res) {
        req.logout();
        req.toastr.success('You have been logged out successfully.', "Success")
        req.session.destroy()

        res.redirect('/login');
    });

    //Authorize
    app.get('/home', common.isLoggedIn, dashboard.dashboardView);

    //     res.render('pages/dashboard');
    // });

    // app.get('/profile', isLoggedIn, (req, res) => {
        
    //     res.render('pages/profile/profile_view');
    // });
    app.get('/profile', common.isLoggedIn, user.getUserProfile);

    app.get('/profile/edit', common.isLoggedIn, user.editProfileView);

    app.get('/profile/changePassword', common.isLoggedIn, (req, res) => {
        var data = req.app.locals.obj;
        req.app.locals.obj = null;
        res.render('pages/profile/profile_edit', {
            data: data ? data.data: {},
            errors: data ? data.errors : {},
            user: {}
        });
    })

    // app.get('/contractor/list', isLoggedIn, (req, res) => {
    //     res.render('pages/contractor/list');
    // })

    // app.get('/contractor/add/:position', isLoggedIn, (req, res) => {
    //     console.log("req.data ===>contractoradd", req.app.locals.obj);
    //     var data = req.app.locals.obj;
    //     req.app.locals.obj = null;
    //     res.render('pages/contractor/add', {
    //         position: req.params.position,
    //         ...data
    //     });
    // })

};