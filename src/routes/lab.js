const lab = require('../controllers/labCtrl');
const states = require('../common/states');
const common = require('../common/common');

module.exports = (app) => {
    app.get('/lab/list/:page', common.isLoggedIn, lab.labList);

    app.get('/lab/add', common.isLoggedIn, (req, res) => {
        res.render('pages/lab/add', {
          states: states
        });
    })

    app.get('/lab/edit/:id/:page', common.isLoggedIn, lab.editView);
    app.get('/lab/view/:id/:page', common.isLoggedIn, lab.labView);
};