const form = require('../controllers/formCtrl');
const common = require('../common/common');

module.exports = (app, passport) => {
    app.get('/form/list', common.isLoggedIn, form.formList);

    app.get('/form/add/:id', common.isLoggedIn, form.getFormById);

    app.post('/update/form', common.isLoggedIn, form.updateForm)
};