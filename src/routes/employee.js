const employee = require('../controllers/employeeCtrl');

const states = require('../common/states');
const common = require('../common/common');

module.exports = function(app) {
    app.get('/employee/list/:page', common.isLoggedIn, employee.employeeList);

    app.get('/employee/add', common.isLoggedIn, (req, res)=> {
        res.render('pages/employee/add', {
          states: states
        });
    });
    app.get('/employee/view/:id/:page', common.isLoggedIn, employee.viewEmployee);
    app.get('/employee/edit/:id/:page', common.isLoggedIn, employee.EditView);
};