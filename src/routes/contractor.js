const user = require('../controllers/userCtrl');
const constructor =require('../controllers/contractorCtrl');
const states = require('../common/states');
const common = require('../common/common');

module.exports = function (app, passport) {
    app.get('/contractor/list/:page', common.isLoggedIn, constructor.contractorList);
    app.get('/contractor/add/:position/:page', common.isLoggedIn, (req, res) => {
        res.render('pages/contractor/add', {
            params: req.params,
            states: states
        });
    })

    app.get('/contractor/view/:id/:page', common.isLoggedIn, constructor.viewContractor);
    app.get('/contractor/edit/:id/:page', common.isLoggedIn, constructor.editContractor)
};