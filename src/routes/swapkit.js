const swapkit = require('../controllers/swapkitCtrl');

const states = require('../common/states');
const common = require('../common/common');

module.exports = function(app) {
    app.get('/swapkit/list/:page', common.isLoggedIn, swapkit.swapList);
    app.get('/swapkit/view/:id/:page', common.isLoggedIn, swapkit.swapKitView);
    app.get('/swapkit/edit/:id/:page', common.isLoggedIn, swapkit.swapKitEdit);
};