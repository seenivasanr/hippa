var jwt = require('jsonwebtoken');

//Validator
var changePass = require('../validationModel/changePasswordM');

//Controller
var user = require('../controllers/userCtrl.js');
var contractor = require('../controllers/contractorCtrl');
var lab = require('../controllers/labCtrl');
var physician = require('../controllers/physicianCtrl');
var patient = require('../controllers/patientCtrl');
var employee = require('../controllers/employeeCtrl');
var swapkit = require('../controllers/swapkitCtrl');
var groupPractice = require('../controllers/groupPracticeCtrl');

var rootMaster ={};
rootMaster.secureRoutes = function (req,res,next) {
    var token = req.body.token || req.headers['x-access-token'] || req.query.token;
    if(token){
        jwt.verify(token, process.env.Key, function(err, decode){
            if(err){
                // If status = expired, prompt user to login again.
                if (err.name === 'TokenExpiredError') {
                    console.log("TokenExpiredError");
                }

                res.status(401).send({
                    msg: 'Invalid token'
                });
            } else {
                next();
            }
        })
    } else {
        res.status(401).send({
            msg: 'No token provider'
        });
    }
};

module.exports = rootMaster;

module.exports = function(app, passport) {
    //Web
        app.post('/api/v1/login', passport.authenticate('local-login', {
            successRedirect : '/home', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));

        // user
        app.post('/api/v1/reset/link', user.sendResetLink);
        app.get('/api/v1/reset/:id/:token', user.redirectPassword);
        app.post('/api/v1/rest/password', user.resetPassword);
        app.post('/api/v1/chnage/password', changePass, user.changePassword);
        app.post('/api/v1/update/profile', user.updateProfile);
        app.post('/api/v1/check/username', user.checkUserName);

        //contractor
        app.post('/api/v1/create/contractor', contractor.createContractor);
        app.post('/api/v1/update/contractor', contractor.updateContractor);
        app.delete('/api/v1/delete/contractor', contractor.deleteContractor);

        //Employee
        app.post('/api/v1/add/employee', employee.addEmployee);
        app.post('/api/v1/update/employee', employee.updateEmployee);
        app.delete('/api/v1/delete/employee', employee.deleteEmployee);

        //lab
        app.post('/api/v1/add/lab', lab.createLab);
        app.post('/api/v1/update/lab', lab.updateLab);
        app.delete('/api/v1/delete/lab', lab.deleteLab);

        //physician
        app.post('/api/v1/add/physician', physician.createPhysician);
        app.post('/api/v1/bulk/create/physician', physician.bulkCreate);
        app.post('/api/v1/check/email', physician.checkAlreadyExitsEmail);
        app.post('/api/v1/update/physician', physician.updatePhysician);
        app.delete('/api/v1/delete/physician', physician.deletePhysician);
        app.post('/api/v1/check/name', physician.checkName);

        //patient
        app.post('/api/v1/add/patient', patient.createPatient);
        app.post('/api/v1/update/patient', patient.updatePatient);
        app.post('/api/v1/update/patient/info', patient.UpdatePatientandUserInfo);
        app.post('/api/v1/update/third/form', patient.updatedThirdForm);
        app.post('/api/v1/check/signature/update', patient.checkSignatureAndUpdate);
        app.delete('/api/v1/delete/patient', patient.deletePatient);
        app.get('/api/v1/send/pdf/:id', patient.sendPDf);

        app.get('/api/v1/signature/link/:id', patient.sendSignatureLink);
        app.get('/api/v1/signature/link/sms/:id', patient.sendSignatureLinkSms);
        app.get('/get/patient/signature/:id/:token', patient.redirectSingaturePage);


        //swapkit
        app.post('/api/v1/update/swapkit', swapkit.updateSwapkit);
        app.delete('/api/v1/delete/swapkit', swapkit.deleteSwapkit);

        //upload
        app.post('/api/v1/upload', patient.uploadFile);
        app.post('/api/v1/upload/profile', user.profileUpload);

        //group practice
        app.post('/api/v1/add/groupPractice', groupPractice.create);
        app.delete('/api/v1/delete/groupPractice', groupPractice.delete)
        app.post('/api/v1/update/groupPractice', groupPractice.update);


};