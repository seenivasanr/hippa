var patient = require('../controllers/patientCtrl');
const common = require('../common/common');


module.exports = (app, passport) => {
    app.get('/custom/form/list', common.isLoggedIn, patient.formList);
    app.get('/patient/add/:testName', common.isLoggedIn, patient.renderPatientForm);
    app.get('/patient/edit/:id/:page', common.isLoggedIn, patient.editForm);
    app.get('/patient/card/edit/:id/:page', common.isLoggedIn, patient.cardEditForm);
    app.get('/patient/list/:page', common.isLoggedIn, patient.getPatienList);
    app.get('/patient/view/:id', common.isLoggedIn, patient.viewPatient);
    app.get('/download/pdf/:id', common.isLoggedIn, patient.downloadPDF);
    app.get('/download/pcf/:id', common.isLoggedIn, patient.downloadPCF);
};