const user = require('../controllers/userCtrl');
const report = require('../controllers/reportCtrl');
const states = require('../common/states');
const common = require('../common/common');

module.exports = function (app, passport) {
    app.get('/reports', common.isLoggedIn, report.viewReports);
    app.get('/reports/contractor/:page', common.isLoggedIn, report.contractorList);
    app.get('/reports/employee/:page', common.isLoggedIn, report.employeeReport);
    app.get('/reports/lab/:page', common.isLoggedIn, report.labReport);
    app.get('/reports/physician/:page', common.isLoggedIn, report.physicianReport);
    app.get('/reports/patients/:page', common.isLoggedIn, report.patientReport);
    app.get('/reports/swapkit/:page', common.isLoggedIn, report.swapkitReport);
    app.get('/reports/newSubmission/:page', common.isLoggedIn, report.newSubmissionReport);
    app.get('/reports/payroll/:page', common.isLoggedIn, report.payrollReport);
    app.get('/reports/resulted/:page', common.isLoggedIn, report.ResultedReport);
    app.get('/reports/holdon/:page', common.isLoggedIn, report.holdOnReport);
    app.get('/reports/insurance/:page', common.isLoggedIn, report.insuranceReport);
    app.get('/reports/adhoc/:page', common.isLoggedIn, report.adHocReport);
};