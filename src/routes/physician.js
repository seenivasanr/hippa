const physician = require('../controllers/physicianCtrl');
const states = require('../common/states');
const common = require('../common/common');

module.exports = (app) => {
    app.get('/physician/list/:page', common.isLoggedIn, physician.physicianList);

    app.get('/physician/add', common.isLoggedIn, (req, res) => {
        res.render('pages/physician/add', {states: states});
    });
    
    app.get('/physician/view/:id/:page', common.isLoggedIn, physician.physicianView);
    app.get('/physician/edit/:id/:page', common.isLoggedIn, physician.editView);
};