const groupPractice = require('../controllers/groupPracticeCtrl');
const states = require('../common/states');
const common = require('../common/common');

module.exports = (app) => {
    app.get('/groupPractice/list/:page', common.isLoggedIn, groupPractice.list);

    app.get('/groupPractice/add', common.isLoggedIn, (req, res) => {
        res.render('pages/groupPractice/add', {
          states: states
        });
    })

    app.get('/groupPractice/edit/:id/:page', common.isLoggedIn, groupPractice.editView);
    app.get('/groupPractice/view/:id/:page', common.isLoggedIn, groupPractice.viewGroup);
};