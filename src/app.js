const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require("fs");
const favicon = require('serve-favicon');
const multiparty = require('connect-multiparty');
var passport = require("passport");
var flash = require("connect-flash");
var toastr = require('express-toastr');
var cookieParser = require("cookie-parser");
const session = require('express-session')
// const redis = require('redis')
// const RedisStore = require('connect-redis')(session);
// let redisClient = redis.createClient()
var moment = require('moment');
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
const config = require('./config/config')[process.env.NODE_ENV || "development"];

var dir = config.uploads_dir_path;
var uploadPath = './images';
var pdfPath = './pdf';
var profilePath = './images/profile';
var signaturePath = './images/signature';
if (!fs.existsSync(uploadPath)){
  fs.mkdirSync(uploadPath);
}

if (!fs.existsSync(pdfPath)) {
  fs.mkdirSync(pdfPath);
}

if (!fs.existsSync(profilePath)) {
  fs.mkdirSync(profilePath);
}

if (!fs.existsSync(signaturePath)) {
  fs.mkdirSync(signaturePath);
}

const multipartyMiddleware = multiparty({ uploadDir: `${uploadPath}` });
const profileUpload = multiparty({ uploadDir: `${profilePath}` });
const signatureUpload = multiparty({ uploadDir: `${signaturePath}` });
const port = config.node_port;

let ejs = require("ejs");


// mongoose.Promise = global.Promise;
app.set("view engine", "ejs");

app.use(favicon(__dirname + '/fav.png'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: true,limit:'50mb'}));
app.use(bodyParser.json({limit:'50mb'}));
app.use(express.static('../../'));
app.use(express.static(__dirname + "/views"));
app.use(express.static(__dirname));
app.use("assets", express.static("/views/assets"));
app.use("images", express.static("/images"));
// app.use(express.static(__dirname + "/views/assets"));
require("./common/passport")(passport);

app.use(flash());
app.use(toastr());

app.use('/api/v1/upload', multipartyMiddleware);
app.use('/api/v1/upload/profile', profileUpload);
app.use('/api/v1/update/patient', signatureUpload);
// app.use(secureRoutes);
// app.use(express.logger("dev")); // log every request to the console
// app.use(express.cookieParser()); // read cookies (needed for auth)

// required for passport
// app.use(session({
//   store: new RedisStore({url: config.redisStore.url}),
//   secret: config.redisStore.secret,
//   resave: false,
//   saveUninitialized: false
// }));
app.use(session({
  // store: new RedisStore({ client: redisClient }),
  secret: "ilovescotchscotchyscotchscotch",
  // cookie: {
    // secure: true,
    // maxAge: new Date(Date.now() + 3600000)
  // },
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(function (req, res, next) {
  // req.currentRoute = req.path;
  res.locals.toasts = req.toastr.render();
  res.locals.session = req.session.user;
  res.locals.currentRoute = req.path;
  res.locals.baseUrl = `${config.host}`;
  res.locals.moment = moment;
  next()
});

var routes = require('./routes/router.js')(app, passport);
require('./routes/pageRouter')(app, passport);
var contractor = require('./routes/contractor.js')(app, passport);
var form = require('./routes/form.js')(app, passport);
var lab = require('./routes/lab.js')(app, passport);
var physician = require('./routes/physician.js')(app, passport);
var patient = require('./routes/patient.js')(app, passport);
var employee = require('./routes/employee.js')(app, passport);
var report = require('./routes/reportRouter.js')(app, passport);
var swapkit = require('./routes/swapkit.js')(app, passport);
var groupPractice = require('./routes/groupPractice.js')(app, passport);

app.get("/", function (req, res) {
  // render to views/index.ejs template file
  res.redirect('/login');
});
// app.use("/contractor", contractor);

app.listen(port,function () {
    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    console.log(" Hippa-SERVER is listening the following port number: " + port);
    console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
});