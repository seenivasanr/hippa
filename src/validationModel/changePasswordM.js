const {check, validationResult} = require("express-validator");

var chnagePassModel = [
    check("oldPassword")
        .trim(),
    check("confirmPassword")
        .isLength({ min: 1 })
        .custom((value, { req, loc, path }) => {
            if (value !== req.body.newPassword) {
                // trow error if passwords do not match
                throw new Error("Passwords don't match");
            } else {
                return value;
            }
        }),
]

module.exports = chnagePassModel;
