var models = require('../db/models');
var Sequelize = require('sequelize');
const Op = Sequelize.Op;
var labMaster = {};

labMaster.findAndCreate = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            models.Labs.create(req).then((data)=> {
                resolve(data.get({plain: true}));
            }).catch(err=> {
                reject(err);
            })
        } catch (err) {
            reject(err);
        }
    })
};

labMaster.findAll = (req, pagination) => {
    return new Promise(async(resolve, reject)=> {
        try {
            var query = {};
            query['where'] = {
                DeletedAt: null,
                email: {
                    [Op.like]: req.email
                },
                labName: {
                    [Op.like]: req.labName
                }
            };
            query['order'] = req['orderBy'];
            query['raw'] = true;
            if (req.export != true) {
                query['offset'] = (pagination.perPage * pagination.page) - pagination.perPage;
                query['limit'] = pagination.perPage;
            }
            const {count, rows} = await models.Labs.findAndCountAll(query);
            if (!rows) {
                reject('No Records Found');
            } else {
                resolve({rows: rows, count: count})
            }
        } catch (err) {
            reject(err);
        }
    })
};

labMaster.findAllList = ()=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Labs.findAll({where: {DeletedAt: null}, raw: true }).then(function (data) {
                resolve(data);
            }).catch(function(err) {
                reject(err);
            });
        } catch (err) {
            reject(err);
        }
    })
};

labMaster.findById = (id)=> {
    return new Promise(async(resolve, reject)=> {
        try {
            var lab = await models.Labs.findByPk(id);
            if (!lab) {
                reject('No Records Found');
            } else {
                resolve(lab.get({plain: true}));
            }
        } catch (err) {
            reject(err);
        }
    })
};

labMaster.findAndUpdate = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            models.Labs.update(req, {where: { id: req.id }}).then(()=> { return models.Labs.findByPk(req.id)})
                .then((lab)=> {
                    if (!lab) {
                        reject('No Records Found');
                    } else {
                        resolve(lab.get({plain: true}));
                    }
                }).catch(err=> {
                    reject(err);
                });
        } catch (err) {
            reject(err);
        }
    })
}

labMaster.findAndDelete = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Labs.destroy({
                where: { id: req.id }
            }).then((data)=> {
                if (!data) {
                    reject('No Records Found');
                } else {
                    resolve(data);
                }
            }).catch(err=> {
                if (err instanceof Sequelize.ForeignKeyConstraintError) {
                    reject('This lab id is referred in patient Table. So we cannot delete');
                } else {
                    reject('No Records Found');
                }
            });
        } catch (err) {
            reject(err);
        }
    })
};


module.exports = labMaster;