var models = require("../db/models");
var formMaster = {};

formMaster.findAll = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            models.Forms.findAll({where: {}, raw: true}).then(data=> {
                if (!data) {
                    reject('No Records Found');
                } else {
                    resolve(data);
                }
            })
        } catch (err) {
            reject(err);
        }
    })
}

formMaster.findOneTest = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            models.Forms.findOne({where: {id: req.id}, raw: true}).then((form)=> {
                if (!form) {
                    reject('No Record Found');
                } else {
                    resolve(form);
                }
            });
        } catch (err) {
            reject(err);
        }
    })
}

formMaster.findAndUpdate = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            var form = models.Forms.update(req.updateData, {
                where: {id: req.id},
                returning: true,
                plain: true
            });
            if (!form) {
                reject('No record Found');
            } else {
                resolve(form);
            }
        } catch (err) {
            reject(err);
        }
    })
}

module.exports = formMaster;