const config = require('../config/config.js')[process.env.NODE_ENV || "development"];

const axios = require('axios');

var swapMaster = {};

swapMaster.orderSave = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            axios({
                method: 'post',
                url: config.swapkit.api_url,
                data: req,
                headers: {
                    "Content-Type":"application/json",
                    "token": config.swapkit.token
                }
              })
                .then(function (response) {
                    console.log("response ", response.data);
                    if (response.data.status == 'Success') {
                        resolve(response.data);
                    } else {
                        reject(response.data);
                    }
                  
                });
              
        } catch (err) {
            reject(err);
        }
    })
};

module.exports = swapMaster;