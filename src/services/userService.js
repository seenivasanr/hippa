var models = require('../db/models');
var moment = require('moment');
var Sequelize = require('sequelize');
const Op = Sequelize.Op;

var userMaster = {};

userMaster.findAndValidate = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            models.Users.findOne({where: {id: req.id}}).then((user)=> {
                if (!user) {
                    reject("No user found");
                }
                
                if (!user.validPassword(req.oldPassword)) {
                    reject('Oops! Wrong password.');
                }
                resolve(user);
            })
        } catch (err) {
            reject(err);
        }
    })
}

userMaster.findOneAndAuth = (req)=> {
    return new Promise(async(resolve, reject) =>{
		try{
            const user = await models.Users.findOne({where: {email: req.email}});
            if (user === null) {
                return reject('No Record Found');
            } else {
                return resolve(user);
            }
		} catch(err) {
			return reject(err);
		}
	})
};

userMaster.findOne = (req) => {
    return new Promise(async(resolve, reject)=> {
        try {
            const user = await models.Users.findOne({where: req}, {plain: true });
            if (user === null) {
                return reject('No Record Found');
            } else {
                return resolve(user.get({plain: true}));
            }
        } catch(err) {
            return reject(err);
        }
    })
}

userMaster.findOneAndUpdate = (req) => {
    return new Promise(async(resolve, reject)=> {
        try {
            var user = await models.Users.update({
                password: req.secret
            }, {
                    where: { id: req.id },
                    returning: true, // needed for affectedRows to be populated
                    plain: true // makes sure that the returned instances are just plain objects
                });
            if (user === null) {
                return reject('No Record Found');
            } else {
                return resolve(user);
            }
        } catch (err) {
            reject(err);
        }
    })
}

userMaster.findAndCreate = (req) => { 
  return new Promise((resolve, reject)=> {
      try {
          models.Users.findOrCreate({where: {userName: req.userName}, defaults: req}).spread(function(user, created){
            if (!created) {
                reject('User Name Already Exists');
            } else {
            resolve(user);
            }
          })
      } catch (err) {
          reject(err);
      }
  })
}

userMaster.findByRole = (req, pagination) => {
 console.log("req ", req);
    return new Promise(async(resolve, reject)=> {
        try {
            
            var searchQuery = {
                role: req.role,
                DeletedAt: null,
                state: {
                    [Op.like]: req.state
                },
                fullName: {
                    [Op.like]: req.fullName
                },
                // gender: {
                //     [Op.like]: req.gender
                // }
            }
            if (req.role == 'employee') {
                searchQuery['status'] = {
                    [Op.like]: req.status
                };
                searchQuery['permission'] = {
                    [Op.like]: req.permission
                };
            }

            if (req.role == 'physician') {
                searchQuery['groupPracticeName'] = {
                    [Op.like]: req.groupPracticeName
                };
            }
            var query = {};
            query['where'] = searchQuery;
            query['order'] = req['orderBy'];
            query['raw'] = true;
            if (req.export != true) {
                query['offset'] = (pagination.perPage * pagination.page) - pagination.perPage;
                query['limit'] = pagination.perPage;
            }
            
            const { count, rows } = await models.Users.findAndCountAll(query);
            // console.log(count);
            // console.log(rows);
            // console.log('length', rows.length);
            if (!rows) {
                reject('No User Found');
            } else {
                resolve({rows: rows, count: count});
            }
            // models.Users.findAll({where: req,
            //     offset: 2,
            //     limit: 1
            // }).then(data=> {
            //     if (!data) {
            //         reject('No User Found');
            //     } else {
            //         resolve(data);
            //     }
            // })
        } catch (err) {
            reject(err);
        }
    })
}

userMaster.findOneAndUpdateProfile = (req) => {
    return new Promise(async(resolve, reject)=> {
        try {
            var user = await models.Users.update(req, {where: { id: req.id }}).then(()=> { return models.Users.findByPk(req.id)})
                .then((user)=> {
                    if (!user) {
                        reject('No Records Found');
                    } else {
                        resolve(user.get({plain: true}));
                    }
                }).catch(err=> {
                    reject(err);
                });
        } catch (err) {
            reject(err);
        }
    })
}

userMaster.findAndDelete = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Users.destroy({
                where: { id: req.id }
            }).then((data)=> {
                if (!data) {
                    reject('No Records Found');
                } else {
                    resolve(data); 
                }
            }).catch(err=> {
                if (err instanceof Sequelize.ForeignKeyConstraintError) {
                    reject('This User id is referred in patient Table. So we cannot delete');
                } else {
                    reject('No Records Found');
                }
            });
        } catch (err) {
            reject(err);
        }
    })
};

userMaster.findAllContractorList = ()=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Users.findAll({where: {DeletedAt: null, role: 'contractor'},attributes: ['id','fullName', 'email', 'constractorId', 'phoneNo', 'role'], raw: true }).then(function (data) {
                resolve(data);
            }).catch(function(err) {
                reject(err);
            });
        } catch (err) {
            reject(err);
        }
    })
};

userMaster.findUserName = (req)=> {
    return new Promise(async(resolve, reject)=> {
        try {
            models.Users.findAll({where: req, raw: true }).then(function (data) {
                if (data.length) {
                    resolve(data);
                } else {
                    reject('No Records Found');
                }
            }).catch(function(err) {
                reject(err);
            });
        } catch (err) {
            reject(err);
        }
    })
}

module.exports = userMaster;