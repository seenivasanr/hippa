var models = require('../db/models');

var Sequelize = require('sequelize');
const Op = Sequelize.Op;

var physicianMaster = {};

physicianMaster.insert = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            models.Physician.create(req).then(data=> {
                resolve(data.get({ plain: true }));
            }).catch(err=> {
                reject(err);
            })
        } catch (err) {
            reject(err);
        }
    })
};

physicianMaster.findAll = (req, pagination) => {
    return new Promise(async(resolve, reject)=> {
        try {
            var query = {};
            query['where'] = {
                DeletedAt: null,
                email: {
                    [Op.like]: req.email
                },
                name: {
                    [Op.like]: req.name
                }
            };
            query['order'] = req['orderBy'];
            query['raw'] = true;
            if (req.export != true) {
                query['offset'] = (pagination.perPage * pagination.page) - pagination.perPage;
                query['limit'] = pagination.perPage;
            }
            const {count, rows} = await models.Physician.findAndCountAll(query)
            if (!rows) {
                reject('No Records Found');
            } else {
                resolve({rows: rows, count: count});
            }
        } catch (err) {
            reject(err);
        }
    })
};

physicianMaster.findAllList = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            req['DeletedAt'] = null;
            models.Physician.findAll({where: req, raw: true }).then(function (data) {
                resolve(data);
            }).catch(function(err) {
                reject(err);
            });
        } catch (err) {
            reject(err);
        }
    })
};

physicianMaster.findById = (id)=> {
    return new Promise(async(resolve, reject)=> {
        try {
            let physician = await models.Physician.findByPk(id);
            resolve(physician ? physician.get({plain: true}): []);
        } catch (err) {
            reject(err);
        }
    })
};

physicianMaster.bulkInsertAndUpdate = (req)=> {
    return new Promise(async(resolve, reject)=> {
        try {
            var newArr = [];
            var primaryPhysicianId = '';
            await req.forEach(async function(elem, i){
                if (elem.id) {
                    var data = await models.Physician.update(
                        elem,
                        {returning: true, raw: true, where: { id:elem.id}}
                      )
                      .then(() => {return models.Physician.findByPk(elem.id);})
                      .then(async function(data) {
                        await newArr.push(data.id);
                        if (elem.physiciantype == 'primary') {
                            primaryPhysicianId = data.id
                        }
                      });
                } else {
                    const physician = await models.Physician.create(elem);
                    console.log("physician's auto-generated ID:", physician.id);
                    await newArr.push(physician.id)
                    if (elem.physiciantype == 'primary') {
                        primaryPhysicianId = physician.id
                    }
                }

                if ((req.length - 1) == i) {
                    resolve({physicianIds: newArr, primaryPhysicianId: primaryPhysicianId})
                }
            })
        } catch (err) {
            reject(err);
        }
    })
}

physicianMaster.findMultipleIds = (ids)=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Physician.findAll({
                where: {
                    id: ids
                },
                raw: true
            }).then(data=> {
                // if(data && data.length > 0) {
                    resolve(data);
                // } else {
                //     reject('No Records Found');
                // }

            })
        } catch (err) {
            reject(err);
        }
    })
};

physicianMaster.bulkCreate = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Physician.bulkCreate(req).then(function(accounts) { // Notice: There are no arguments here, as of right now you'll have to...
                resolve(accounts);
              }).catch(err=> {
                  reject(err);
              })
        } catch (err) {
            reject(err);
        }
    })
}

physicianMaster.findAndUpdate = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            models.Physician.update(req, {where: { id: req.id }}).then(()=> { return models.Physician.findByPk(req.id)})
                .then((data)=> {
                    if (!data) {
                        reject('No Records Found');
                    } else {
                        resolve(data.get({plain: true}));
                    }
                }).catch(err=> {
                    reject(err);
                });
        } catch (err) {
            reject(err);
        }
    })
}

physicianMaster.findAndDelete = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Physician.destroy({
                where: { id: req.id }
            }).then((data)=> {
                if (!data) {
                    reject('No Records Found');
                } else {
                    resolve(data);
                }
            }).catch(err=> {
                if (err instanceof Sequelize.ForeignKeyConstraintError) {
                    reject('This physician id is referred in patient Table. So we cannot delete');
                } else {
                    reject('No Records Found');
                }
            });
        } catch (err) {
            reject(err);
        }
    })
};

physicianMaster.findPhysicianName = (req)=> {
    return new Promise(async(resolve, reject)=> {
        try {
            models.Physician.findAll({where: req, raw: true }).then(function (data) {
                if (data.length) {
                    resolve(data);
                } else {
                    reject('No Records Found');
                }
            }).catch(function(err) {
                reject(err);
            });
        } catch (err) {
            reject(err);
        }
    })
}


module.exports = physicianMaster;