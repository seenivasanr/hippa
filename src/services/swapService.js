var models = require('../db/models');

var Sequelize = require('sequelize');
const Op = Sequelize.Op;

var swapMaster = {};

swapMaster.findById = (req) => {
    console.log("req ==>findById", req);
    return new Promise(async(resolve, reject)=> {
        try {
            models.Swapkit.findOne({
                where: req
            }).then(data=> {
                resolve(data == null ? false: true);
            }).catch(err=> {
                resolve(false);
            });
            
        } catch (err) {
            reject(err);
        }
    })
};

swapMaster.findAndCreate = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            models.Swapkit.create(req).then((data)=> {
                resolve(data.get({plain: true}));
            }).catch(err=> {
                reject(err);
            })
        } catch (err) {
            reject(err);
        }
    })
};

swapMaster.findAll = (req, pagination) => {
    return new Promise(async(resolve, reject)=> {
        try {
            var query = {};
            query['where'] = {
                DeletedAt: null,
                orderStatus: {
                    [Op.like]: req.orderStatus
                },
                // labName: {
                //     [Op.like]: req.labName
                // }
            };
            
            if (req['orderBy'][0] && req['orderBy'][0][2] == 'patientModel') {
                query['order'] = [
                    [{ model: models.Patient, as: 'patient' }, req['orderBy'][0][0], req['orderBy'][0][1]]
                ];
            } else {
                query['order'] = req['orderBy'];
            }
            
            query['raw'] = true;
            query['include'] = [
                {
                    model: models.Patient,
                    as: 'patient',
                    where: {
                        [Op.and]: [
                            Sequelize.where(Sequelize.fn('concat', Sequelize.col('firstName'),' ', Sequelize.col('lastName')), {
                                [Op.like]: req.patientName
                            })
                        ]
                    }
                },
                {
                    model: models.Users,
                    as: 'agent',
                    where: {
                        fullName: {
                            [Op.like]: req.contractorName
                        }
                    }
                },
            ];
            
            if (req.export != true) {
                query['offset'] = (pagination.perPage * pagination.page) - pagination.perPage;
                query['limit'] = pagination.perPage;
            }
            const {count, rows} = await models.Swapkit.findAndCountAll(query);
            if (!rows) {
                reject('No Records Found');
            } else {
                resolve({rows: rows, count: count})
            }
        } catch (err) {
            reject(err);
        }
    })
};

swapMaster.findOne = (id)=> {
    return new Promise(async(resolve, reject)=> {
        try {
            var swap = await models.Swapkit.findOne({
                where: {
                    id: id,
                },
                include: [
                    {
                        model: models.Patient,
                        as: 'patient'
                    },
                    {
                        model: models.Users,
                        as: 'agent'
                    }
                ]
            });
            if (!swap) {
                reject('No Records Found');
            } else {
                resolve(swap.get({plain: true}));
            }
        } catch (err) {
            reject(err);
        }
    })
};

swapMaster.findAndUpdate = (req) => {
    return new Promise((resolve, reject)=> {
        try {
            models.Swapkit.update(req, {where: { id: req.id }}).then(()=> { return models.Swapkit.findByPk(req.id)})
                .then((data)=> {
                    if (!data) {
                        reject('No Records Found');
                    } else {
                        resolve(data.get({plain: true}));
                    }
                }).catch(err=> {
                    reject(err);
                });
        } catch (err) {
            reject(err);
        }
    })
}

swapMaster.findAndGroup = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Swapkit.findAll({
                where: {},
                group: ['orderStatus'],
                attributes: ['orderStatus', [Sequelize.fn('COUNT', Sequelize.col('orderStatus')), 'statusCount']],
                raw: true
            }).then(function (tags) {
                resolve(tags);
                });
        } catch (err) {
            reject(err);
        }
    })
};

swapMaster.findAndDelete = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Swapkit.destroy({
                where: { id: req.id }
            }).then((data)=> {
                if (!data) {
                    reject('No Records Found');
                } else {
                    resolve(data);
                }
            }).catch(err=> {
                reject(err);
            });
        } catch (err) {
            reject(err);
        }
    })
};
   

module.exports = swapMaster;