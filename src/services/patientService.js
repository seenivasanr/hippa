var models = require('../db/models');
var moment = require('moment');
var Sequelize = require('sequelize');
const Op = Sequelize.Op;

var patientMaster = {};

patientMaster.findAndCreate = (req) => {
    return new Promise((resolve, reject)=> {
        try { 
            // models.Patient.findOrCreate({where: {email: req.email}, defaults: req,raw: true}).spread((user, created)=> {
            models.Patient.create(req).then((data)=> {
                resolve(data.get({plain: true}));  
                // if (!created) {
                //     reject('Email Id Already Exists');
                // } else {
                //     resolve(user.get({plain: true}));
                // }
          })
        } catch (err) {
            reject(err);
        }
    })
};

patientMaster.findAndUpdate = (req) => {
    return new Promise(async(resolve, reject)=> {
        try {
            models.Patient.update(req,{
                    where: { id: req.id }
                }).then(()=> { return models.Patient.findOne({
                    include: [
                        {
                            model: models.Labs,
                            as: 'lab'
                        },
                        {
                            model: models.Physician,
                            as: 'physician'
                        },
                        {
                            model: models.Users,
                            as: 'agent'
                        }
                    ],
                    where: {id: req.id}
                });})
                .then((user)=> {
                    if (!user) {
                        reject('No Records Found');
                    } else {
                        resolve(user.get({plain: true}));
                    }
                }).catch(err=> {
                    reject(err);
                });
            
        } catch (err) {
            reject(err);
        }
    })
}

patientMaster.findAll = (req, pagination) => {
    return new Promise(async(resolve, reject)=> {
        try {
            var searchObj = {};
            if (req.role == 'contractor') {
                searchObj['agentId'] = req.agentId;
            } 
            else if (req.role == 'physician') {
                searchObj['createrId'] = req.createrId;
            }
            var searchQuery = {
                // name: {
                //     [Op.like]: req.name
                // },
                testStatus: {
                    [Op.like]: req.testStatus
                }
                // gender: {
                //     [Op.like]: req.gender
                // }
            }
            if (req.formStatus) {
                searchQuery['formStatus'] = {
                    [Op.like]: req.formStatus
                 }
            }
            searchQuery['DeletedAt'] = null;
            if (req['submissionDate']) {
                var date = moment(new Date(req['submissionDate']), 'MM-DD-YYYY');
                searchQuery['submissionDate'] = {
                    [Op.gte]: date.toDate(),
                    [Op.lte]: date.add(1, 'days').toDate()
                }
            }

            if (req['dob']) {
                var date = moment(new Date(req['dob']), 'MM-DD-YYYY');
                searchQuery['dob'] = {
                    [Op.gte]: date.toDate(),
                    [Op.lte]: date.add(1, 'days').toDate()
                }
            }

            if(req.notPaidStatus == true) {
                searchQuery['testStatus'] = {
                    [Op.not]: 'Paid'
                }
            }
            
            searchObj[Op.or] = [
                searchQuery
                
            ]
            searchObj[Op.and] = [
                Sequelize.where(Sequelize.fn('concat', Sequelize.col('firstName'),' ', Sequelize.col('lastName')), {
                    [Op.like]: req.name
                })
            ]
            console.log("searchObj ", searchObj);
            var query = {};
            query['where'] = searchObj;
            query['order'] = req['orderBy'];
            query['raw'] = true;
            query['include'] = [
                {
                    model: models.Users,
                    as: 'agent',
                    where: {
                        fullName: {
                            [Op.like]: req.contractorName
                        }
                    }
                }
            ];
            if (req.export != true) {
                query['offset'] = (pagination.perPage * pagination.page) - pagination.perPage;
                query['limit'] = pagination.perPage;
            }
            const {count, rows} = await models.Patient.findAndCountAll(query)
            if (!rows) {
                reject('No Records Found');
            } else {
                resolve({rows: rows, count: count});
            }
        } catch (err) {
            reject(err);
        }
    })
};

patientMaster.findById = (id)=> {
    return new Promise(async(resolve, reject)=> {
        try {
            let patient = await models.Patient.findOne({
                include: [
                    {
                        model: models.Labs,
                        as: 'lab'
                    },
                    {
                        model: models.Physician,
                        as: 'physician'
                    },
                    {
                        model: models.Users,
                        as: 'agent'
                    }
                ],
                where: {id: id}
            });
            if (!patient) {
                reject('No Patient Found');
            } else{
                resolve(patient.get({ plain: true }));
            }
        } catch (err) {
            reject(err);
        }
    })
};

patientMaster.findAndDelete = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            models.Patient.destroy({
                where: { id: req.id }
            }).then((data)=> {
                console.log("data ", data);
                if (!data) {
                    reject('No Records Found');
                } else {
                    resolve(data);
                }
            }).catch(err=> {
                reject('No Records Found');
            });
        } catch (err) {
            reject(err);
        }
    })
};

patientMaster.findAndGroup = (req)=> {
    return new Promise((resolve, reject)=> {
        try {
            req['formStatus'] = 'Complete'
            models.Patient.findAll({
                where: req,
                group: ['testStatus'],
                attributes: ['testStatus', [Sequelize.fn('COUNT', Sequelize.col('testStatus')), 'statusCount']],
                raw: true
            }).then(function (tags) {
                resolve(tags);
              });
        } catch (err) {
            reject(err);
        }
    })
};

patientMaster.findAndGroupPatient = (req, pagination) => {
    return new Promise(async(resolve, reject)=> {
        try {
            var searchObj = {};
            var searchQuery = {
                testStatus: {
                    [Op.like]: req.testStatus
                },
                formStatus: 'Complete',
                //agentId: req.contractorId,
                // labId: req.labId,
                //physicianId: req.physicianId
            }
            if (req.labId) {
                searchQuery['labId'] = req.labId;
            }
            if (req.contractorId) {
                searchQuery['agentId'] = req.contractorId;
            }
            if (req.physicianId) {
                searchQuery['physicianId'] = req.physicianId;
            }
            
            if (req.daterange) {
                var pickedDate = req.daterange.split('-');
                searchQuery['submissionDate'] = {
                    [Op.gte]: new Date(pickedDate[0]),
                    [Op.lt]: new Date(pickedDate[1])
                };
            }
            
            searchQuery['DeletedAt'] = null;
            
            searchObj[Op.or] = [
                searchQuery
            ]
            // searchObj[Op.and] = [
            //     searchQuery,
            //     // Sequelize.where(
            //     //     Sequelize.cast(Sequelize.col('labId'), 'text'),
            //     //     {[Op.iLike]: '1'}
            //     // ),
            // ]
            console.log("searchObj ", searchObj);
            var query = {};
            query['where'] = searchQuery;
            // query['order'] = req['orderBy'];
            query['raw'] = true;
            query['include'] = [
                {
                    model: models.Users,
                    as: 'agent'
                },
                {
                    model: models.Labs,
                    as: 'lab'
                }
            ];
            // query['group']= ['agentId', Sequelize.fn('month', Sequelize.col('submissionDate'))]
            // query['attributes']= ['agentId', 'submissionDate','firstName', 'lastName', 'fullName',[Sequelize.fn("concat", Sequelize.col("firstName"), ' ', Sequelize.col("lastName")), 'fullName'],
            // 'agent.state', 'dob', 'testStatus'];
            // if (req.export != true) {
            //     query['offset'] = (pagination.perPage * pagination.page) - pagination.perPage;
            //     query['limit'] = pagination.perPage;
            // }
            const {count, rows} = await models.Patient.findAndCountAll(query)
            if (!rows) {
                reject('No Records Found');
            } else {
                // var data = [];
                // if (req.export == true) {
                    // data = rows.filter(item=> {
                    //     item['fullName'] = `${item.firstName} ${item.lastName}`
                    //     var insuranceInfo = JSON.parse(item.insuranceInfo);
                    //     item['primaryInsuranceType'] = insuranceInfo[0].insuranceName == 'Primary Insurance' ? insuranceInfo[0].insuranceType: insuranceInfo[1].insuranceType;
                    //     return item;
                    // })

                    var obj = {};
                    var newDate = new Date();
                    rows.forEach(element => {
                        if (obj[element.agentId] == undefined) {
                            obj[element.agentId] = {
                                //count: 1,
                                thisMonth: 0,
                                lastMonth: 0,
                                thisYear: 0,
                            }
                        } 
                        // else {
                        // 	obj[element.agentId].count += 1
                        // }

                        if (newDate.getMonth()+1 == new Date(element.submissionDate).getMonth()+1) {
                            obj[element.agentId].thisMonth += 1;
                        }

                        if (newDate.getMonth() == new Date(element.submissionDate).getMonth()+1) {
                            obj[element.agentId].lastMonth += 1;
                        }

                        if (newDate.getFullYear()  == new Date(element.submissionDate).getFullYear()) {
                            obj[element.agentId].thisYear += 1;
                        }
                        obj[element.agentId].partnerName= element['agent.partnerName'];
                        obj[element.agentId].partnerId= element['agent.partnerId'];
                        obj[element.agentId].fullName= element['agent.fullName'];
                        obj[element.agentId].state= element['agent.state'];
                    });
                    var data = Object.entries(obj).map(item => (item[1]));
                // } else {
                //     data = rows;
                // }
                resolve({rows: data, count: count});
            }
        } catch (err) {
            reject(err);
        }
    })
};

patientMaster.findPayrollReport = (req, pagination) => {
    return new Promise(async(resolve, reject)=> {
        try {
            var searchObj = {};
            var searchQuery = {
                testStatus: {
                    [Op.like]: req.testStatus
                },
                formStatus: 'Complete'
            }
            if (req.labId) {
                searchQuery['labId'] = req.labId;
            }
            if (req.contractorId) {
                searchQuery['agentId'] = req.contractorId;
            }
            if (req.physicianId) {
                searchQuery['physicianId'] = req.physicianId;
            }

            if (req.state == 'insurance') {
                searchQuery['testStatus'] = {
                    [Op.not]: 'Paid'
                };
                searchQuery['preInsuranceType']= {
                    [Op.like]: req.preInsuranceType
                };
            }

            if (req.daterange) {
                var pickedDate = req.daterange.split('-');
                searchQuery['submissionDate'] = {
                    // [Op.gte]: new Date(pickedDate[0]),
                    // [Op.lte]: new Date(pickedDate[1])
                    [Op.between]: [new Date(pickedDate[0]), new Date(pickedDate[1])]
                };
            }

            searchQuery['DeletedAt'] = null;
            
            searchObj[Op.or] = [
                searchQuery
            ]
            console.log("searchObj ", searchObj);
            var query = {};
            query['where'] = searchQuery;
            if (req['orderBy'][0] && req['orderBy'][0][2] == 'userModel') {
                query['order'] = [
                    [{ model: models.Users, as: 'agent' }, req['orderBy'][0][0], req['orderBy'][0][1]]
                ];
            } else if(req['orderBy'][0] && req['orderBy'][0][2] == 'Physician') {
                query['order'] = [
                    [{ model: models.Physician, as: 'physician' }, req['orderBy'][0][0], req['orderBy'][0][1]]
                ];
            } else {
                query['order'] = req['orderBy'];
            }
            query['raw'] = true;
            query['include'] = [
                {
                    model: models.Users,
                    as: 'agent'
                },
                {
                    model: models.Physician,
                    as: 'physician'
                },
                {
                    model: models.Labs,
                    as: 'lab'
                }
            ];
            query['attributes']= ['firstName', 'lastName', 'dob', 'phoneNo', [Sequelize.fn("concat", Sequelize.col("firstName"), ' ', Sequelize.col("lastName")), 'fullName'],
             'preInsuranceType', 'state', 'testStatus', 'approvalPendingDate','doctorApproved',
            'accepted','onHold','shippedtoLab','receivedbyLab','inProgressatLab', 'resulted', 'submissionDate'];
            if (req.export != true) {
                query['offset'] = (pagination.perPage * pagination.page) - pagination.perPage;
                query['limit'] = pagination.perPage;
            }
            const {count, rows} = await models.Patient.findAndCountAll(query)
            if (!rows) {
                reject('No Records Found');
            } else {
                resolve({rows: rows, count: count});
            }
        } catch (err) {
            reject(err);
        }
    })
};

module.exports = patientMaster;