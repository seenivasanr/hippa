var styles = {											// Styles
    headerDark: {
        fill: {
            fgColor: {
                rgb: 'ef9035'
            }
        },
        font: {
            color: {
                rgb: 'FFFFFFFF'
            },
            sz: 14,
            bold: true,
            // underline: true
        }
    },
    coloumnHeader: {
        fill: {
            fgColor: {
                rgb: 'f5ae6b'
            }
        },
        font: {
            color: {
                rgb: '000'
            },
            sz: 12,
            bold: true
        }
    },
};
module.exports = styles;