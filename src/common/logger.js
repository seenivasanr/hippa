var log = require('intel');
var errorDb = require('../models/errorModel');

log.error = (error, type) => {
    try {
        type = type || "error";
        error = error.stack || error
        console.log('\x1b[31m',"========+ERROR+=======")
        console.log(error);
        console.log(" ======================",'\x1b[0m')
        errorModel = new errorDb({
            level: "error",
            text: error,
            date: Date.now(),
            type:type
        })
        errorModel.save().then(data=>console.log(data), err=>console.log(err))
    } catch (err) {
        console.log("log.error: "+err)
    }
}

log.warn= (error, type) => {
    try {
        type = type || "warning";
        console.log('\x1b[36m',"========+WARNING+=======")
        console.log(error)
        console.log(" ========================",'\x1b[0m')

        errorModel = new errorDb({
            level: "warning",
            text: error,
            date: Date.now(),
            type:type
        })
        errorModel.save().then(data=>console.log(data), err=>console.log(err))
    } catch (err) {
        console.log("log.warn"+err)
    }
}
exports.log = log;