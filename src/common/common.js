const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
const fs = require('fs');
let pdf = require("html-pdf");
const config = require('../config/config')[process.env.NODE_ENV || "development"];

var userService = require('../services/userService.js');
const { resolve } = require('path');
const { reject } = require('lodash');

var common = {};

common.genHash = function(pass) {
	return new Promise((resolve, reject)=> {
		bcrypt.hash(pass, 8, function (err, hash) {
			if(err) return reject(err);
	        resolve(hash); 
	    });
	})
};

common.compareHash = function(pass, userPass) {
	return new Promise((resolve, reject)=> {
		bcrypt.compare(pass, userPass).then(res => {
		    if(res==false) return reject(err);
		    resolve('success');
		});
	})
};


// create reusable transporter object using the default SMTP transport

// if (process.env.NODE_ENV == 'development' || process.env.NODE_ENV == 'test') {
//  console.log("process.env.NODE_ENV if", process.env.NODE_ENV);
// 	var transporter = nodemailer.createTransport({
// 		service: "Gmail",
// 		auth: {
// 			user: config.mailer.id,
// 			pass: config.mailer.password
// 		}
// 	});
// } else {
	console.log("process.env.NODE_ENV else  ", process.env.NODE_ENV);
	
	var transporter = nodemailer.createTransport({
		host: config.mailer.host,
		port: config.mailer.port,
		auth: {
			user: config.mailer.id,
			pass: config.mailer.password
		},
		debug: true, // show debug output
  		logger: true // log information in console
	});

// }

//  console.log("config ", config);

transporter.verify(function(error, success) {
	if (error) {
	  console.log('transporter',error);
	} else {
	  console.log("Server is ready to take our messages");
	}
});
// console.log("transporter ", transporter);

common.sendMail = function(req) {
	return new Promise((resolve, reject)=> {
		var mailOptions = {
			from: '"Genetix HIPAA" <'+config.mailer.id+'>',
				to: req.to,
				cc: config.mailer.ccId,
		    // to: 'seenivasan.ramakrishnan@colanonline.com',
		    subject: req.subject,
		    html: req.html
		};

		if (req["filename"]) {
			mailOptions['attachments'] = [{filename: 'Requisition.pdf',path: req['filepath'],contentType: 'application/pdf'}]
		}

		console.log("mailOptions ", mailOptions);
		transporter.sendMail(mailOptions, function(err, res){
 console.log("res ", res);
 console.log("err ", err);
	    	if(err || res==false) return reject(err);
		    resolve(res);
		});
	})
};

common.fileUpload = function(files, path){
	return new Promise((resolve, reject)=> {
		fs.readFile(files.file.path, function (err, data) {
		  	var newPath = path;
		  	fs.writeFile(newPath, data, function (err) {
		  		if(err){ return reject(err); }
		    	resolve("done");
		  	});
		});
	})
};

common.createPdf = function(html, fileName) {
	return new Promise((resolve, reject)=> {
		let width = 850, height = 1500;
		let options = {
            width: `${width}px`,
        	height: `${height}px`,
        };

		pdf.create(html, options).toFile(`./pdf/${fileName}`, function(err, res) {
			if (err) reject(err);
			resolve(res);
		});
	})
};

common.isLoggedIn = function(req, res, next){
	if (req.isAuthenticated()) {
		if (req.session.user.role == 'employee') {
			userService.findOne({id:req.session.user.id}).then(resp=> {
				if (resp.permission != req.session.user.permission || resp.isActive != true) {
					req.logout();
					req.toastr.error("Admin has changed the permissions", "Error");
					setTimeout(() => {
						res.redirect("/login");
					}, 1000);
				} else {
					return next();
				}
			},err=> {
				res.redirect(404," /login");
			})
		} else{
			return next();
		}
	} else {
		res.redirect("/login");
	}
}

// require the Twilio module and create a REST client
const smsClient = require('twilio')(config.sms.accountSid, config.sms.authToken);

common.sendSMS = (req)=> {
	return new Promise((resolve, reject)=> {
		smsClient.messages.create({
			body: req['body'],
			to: req['to'],
			from: config.sms.no
		}).then(message => {
			console.log(message);
			resolve(message);
		 }).catch(err => {
			console.log('error', err);
			reject(err)
		})
	})
}
 
module.exports = common;