const os = require('os');

var config = {};
////////////////////////////////////////////////////////////////////
process.env.NODE_ENV = "development";
//    process.env.NODE_ENV = "test"
// process.env.NODE_ENV = "production"
/////////////////////////////////////////////////////////////////////

config.development = {
	uploads_dir_path : `${os.homedir()}/hippa_uploads`,
	// uploads_dir_path : `${os.homedir()}/uploads`,
	host : "http://localhost:8182",
	node_port : 8182,
	mailer : {
		// id : "gayathri-123",
		// password : "gayathri@123",
		// host: "smtp.sendgrid.net",
		// port: 587,
		id : "seenivrp@gmail.com",
		password : "Welcome@5477",
		host: "smtp.gmail.com",
		port: 465,
		ccId: [],
		pathTech: 'seenivasan.ramakrishnan@colanonline.com',
	},
	swapkit: {
		"api_url": "https://api.labsupplyline.com/api/OrderSave_L1_Test", //Test
		"token": "73a3a918-c85a-49f9-8d63-a0610bfcacc5", //Test
		"clientID":"1674",
		"country":"US",
		"outboundShippingServiceLevel":"FedEx Ground", //FedEx Ground, UPS Ground, UPS Next Day Air
		"items":[
			{
			"itemNumber":"7858",
			"quantity":"1"
			}
		],
	},
	sms: {
		accountSid: 'AC39c5c7f51e68fc1180be9d4ee15190a8',
		authToken: '2d08861ed5ffcece8dad007d1a52c1e1',
		no: '+12319303129'
	},
	secret_key: 'hippa_web_dev_1582719031'
};

config.test = {
	uploads_dir_path : `${os.homedir()}/hippa_uploads`,
	host : "https://genetix.colanonline.in",
	node_port : 8898,
	mailer : {
		// id : "gayathri-123",
		// password : "gayathri@123",
		// host: "smtp.sendgrid.net",
		// port: 587,
		id : "cipl.test2020@gmail.com",
		password : "Welcome123!",
		host: "smtp.gmail.com",
		port: 587,
		ccId: 'test@colanonline.com',
		pathTech: 'test@colanonline.com',
		// id : "seenivrp@gmail.com",
		// password : "Welcome@5477"
	},
	swapkit: {
		"api_url": "https://api.labsupplyline.com/api/OrderSave_L1_Test", //Test
		"token": "73a3a918-c85a-49f9-8d63-a0610bfcacc5", //Test
		"clientID":"1674",
		"country":"US",
		"outboundShippingServiceLevel":"FedEx Ground", //FedEx Ground, UPS Ground, UPS Next Day Air
		"items":[
			{
			"itemNumber":"7858",
			"quantity":"1"
			}
		],
	},
	sms: {
		accountSid: 'ACc27eb5578f72d4fbe53aa67abd920cb8',
		authToken: 'cf82888c8ad5145502b6dd0e7eb571c3',
		no: '+12053521952'
	},
	secret_key: 'hippa_web_test_1582719031'
};

config.production = {
	uploads_dir_path : `${os.homedir()}/hippa_uploads`,
	host : "https://genetixdnascreening.com",
	node_port : 8899,
	mailer : {
		id : "info@genetixdnascreening.com",
		password : "[@0PeMjdGjgm",
		host: "mail.genetixdnascreening.com",
		port: 465,
		ccId: 'roger@dnaprojectconsulting.com',
		pathTech: 'roger@dnaprojectconsulting.com',
	},
	swapkit: {
		"api_url": "https://api.labsupplyline.com/api/OrderSave_L1", //Live
		"token": "dcce265b-ae89-4444-b58e-83f431360c79", //Live
		"clientID":"1674",
		"country":"US",
		"outboundShippingServiceLevel":"FedEx Express Priority", //FedEx Express Priority, FedEx Ground, UPS Ground
		"items":[
			{
			"itemNumber":"7858",
			"quantity":"1"
			}
		],
	},
	sms: {
		accountSid: 'ACc27eb5578f72d4fbe53aa67abd920cb8',
		authToken: 'cf82888c8ad5145502b6dd0e7eb571c3',
		no: '+12053521952'
	},
	secret_key: 'hippa_web_pro_1582719031'
};

module.exports = config;