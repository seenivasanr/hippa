var physicianService = require('../services/physicianService');

var states = require('../common/states');

var physicianMaster = {};

physicianMaster.createPhysician = (req, res) => {
    physicianService.insert(req.body).then(resp=> {
        req.toastr.success("Created Successfully", "Success");
        res.redirect('/physician/list/1');
    }).catch(err=> {
        req.toastr.error("Email Id Already Exists", "Error");
        res.redirect("/physician/add");
        // res.render('pages/physician/add', {
        //     data: req.body,
        //     states: states
        // });
    })
};

physicianMaster.physicianList = (req, res) => {
    var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['name'] = `%${req.query['name']? req.query['name']: ''}%`;
    reqObj['email'] = `%${req.query['email']? req.query['email']: ''}%`;
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    physicianService.findAll(reqObj, pagination).then(resp=> {
        res.render('pages/physician/list', {
            data: resp.rows,
            current: pagination['page'],
            pages: Math.ceil(resp.count / pagination['perPage']),
            perPage: pagination['perPage'],
            name : req.query['name'],
            email : req.query['email'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
        })
    }, err=> {
        res.render('pages/physician/list', {
            data: [],
            current: 1,
            pages: 0,
            name : req.query['name'],
            email : req.query['email'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
        })
    })
};

physicianMaster.bulkCreate = (req, res)=> {
    physicianService.bulkCreate(req.body.data).then(resp=> {
        res.send({
            status: true,
            data: resp,
            msg: 'Created successfully'
        })
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wroung'
        })
    })
};

physicianMaster.checkAlreadyExitsEmail = (req, res)=> {
    physicianService.findAllList(req.body).then(resp=> {
        res.send({
            status: true,
            data: resp,
            msg: 'Email Already Exists'
        });
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'Bad Request'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wroung'
        })
    })
};

physicianMaster.physicianView = (req, res) => {
    console.log("req.params ", req.params);
    physicianService.findById(req.params.id).then(resp=> {
        console.log("resp ==>physicianView", resp);
        res.render('pages/physician/view', {
            data: resp,
            states: states,
            params: req.params
        })
    },err=> {
        res.redirect(400, '/physician/list/1');
    }).catch(err=> {
        res.redirect(500, '/physician/list/1');
    })
        
};

physicianMaster.editView = (req, res) => {
    console.log("req.params ", req.params);
    physicianService.findById(req.params.id).then(resp=> {
        console.log("resp ==>editViewphysician", resp);
        res.render('pages/physician/edit', {
            data: resp,
            states: states,
            params: req.params
        })
    },err=> {
        res.redirect(400, '/physician/list/1');
    }).catch(err=> {
        res.redirect(500, '/physician/list/1');
    })
};

physicianMaster.updatePhysician = (req, res)=> {
    physicianService.findAndUpdate(req.body).then(resp=> {
        res.send({
            status: true,
            data:resp,
            msg: "Updated Successfully"
        })
    }, err=> {
        res.status(400).send({
            status: false,
            data: {},
            msg: 'Invalid Request'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

physicianMaster.deletePhysician = (req, res)=> {
    var reqObj = {};
    reqObj['id'] = req.query.id;
    physicianService.findAndDelete(reqObj).then(resp=> {
        res.send({
            status: true,
            msg: 'Deleted Successfully'
        });
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: err
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

physicianMaster.checkName = (req, res)=> {
    physicianService.findPhysicianName(req.body).then(resp=> {
        console.log("resp ", resp);
        res.send({
            status: true,
            data: resp,
            msg: 'Physician name is already exists'
        });
    }, err=> {
        res.send({
            status: false,
            data: {},
            msg: err
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
}

module.exports = physicianMaster;