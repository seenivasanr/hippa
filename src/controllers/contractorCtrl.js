var userService = require('../services/userService');

var common = require("../common/common.js");
var states = require("../common/states");
const config = require('../config/config')[process.env.NODE_ENV || "development"];

const ejs = require("ejs");
const fs = require("fs");
const randomstring = require("randomstring");

var contractorMaster = {};

contractorMaster.contractorList = (req, res) => {
	var reqObj = {};
	var pagination = {};
	pagination['perPage'] = 10;
	pagination['page'] = req.params.page || 1
	reqObj['role'] = 'contractor';
	// reqObj['gender'] = `%${req.query['gender']? req.query['gender']: ''}%`;
	reqObj['fullName'] = `%${req.query['fullName']? req.query['fullName']: ''}%`;
	reqObj['email'] = `%${req.query['email']? req.query['email']: ''}%`;
	reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
	reqObj['state'] = `%${req.query['state']? req.query['state']: ''}%`;
	reqObj['groupPracticeName'] = `%${req.query['groupPracticeName']? req.query['groupPracticeName']: ''}%`;

  console.log("req.query ===>", req.query);
	userService.findByRole(reqObj, pagination).then(resp=> {
		//  console.log("resp ", resp);
		res.render('pages/contractor/list', {
			data: resp.rows,
			current: pagination['page'],
			pages: Math.ceil(resp.count / pagination['perPage']),
      perPage: pagination['perPage'],
      email : req.query['email'],
      gender : req.query['gender'],
      fullName : req.query['fullName'],
      orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
      queryUrl: req._parsedOriginalUrl.search
    });
	}, err=> {
		res.render('pages/contractor/list', {
			data: [],
			current: 1,
      pages: 1,
      email : req.query['email'],
      gender : req.query['gender'],
      fullName : req.query['fullName'],
      orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
      queryUrl: req._parsedOriginalUrl.search
		})
	})
}

contractorMaster.createContractor = (req, res) => {
  var data = req.body;
  !data['dob'] && delete data['dob'];
  console.log("data ==>createContractor", data);
  //  return false;
  var random = randomstring.generate(6);
 console.log("random ", random);
  common.genHash(random).then(secret => {
    data["password"] = secret;

    userService.findAndCreate(data).then(resp => {
      var filePath = "views/pages/template/password.ejs";
      var compiled = ejs.compile(fs.readFileSync(filePath, "utf8"));
      var mailObj = {};
      mailObj["to"] = data["email"];
      mailObj["subject"] = "Genetix HIPAA - Welcome";
      mailObj["html"] = compiled({data: data, random: random, baseUrl: config.host});
      common.sendMail(mailObj);
      req.toastr.success("Created Successfully", "Success");
      res.redirect("/contractor/list/1");
    }, err => {
      req.toastr.error("User Name Already Exists", "Error");
      req.app.locals.obj = req.body;
      // res.redirect("/contractor/list/1");
      res.redirect(`/contractor/add/${req.body.position}/1`);
    });
  });
};

contractorMaster.viewContractor = (req, res)=> {
  userService.findOne({id: req.params.id}).then(resp=> {
    console.log("resp viewContractor", resp);
    res.render('pages/contractor/view',{
      data: resp
    })
  }, err=> {
    res.redirect(404,'/contractor/list/1');
  }).catch(err=> {
    res.redirect(500,'/contractor/list/1');
  })
};

contractorMaster.editContractor = (req, res)=> {
  userService.findOne({id: req.params.id}).then(resp=> {
    res.render('pages/contractor/edit',{
      data: resp,
      params: req.params,
      states: states
    })
  }, err=> {
    res.redirect(404,`/contractor/list/${req.params.page}`);
  }).catch(err=> {
    res.redirect(500,`/contractor/list/${req.params.page}`);
  })
};

contractorMaster.updateContractor = (req, res)=> {
  !req.body['dob'] && delete req.body['dob'];
  userService.findOneAndUpdateProfile(req.body).then(resp=> {
    res.send({
      status: true,
      data:resp,
      msg: "Updated Successfully"
    })
  }, err=> {
    res.status(400).send({
      status: false,
      data: {},
      msg: 'Invalid Request'
    });
  }).catch(err=> {
    res.status(500).send({
      status: false,
      data: {},
      msg: 'Something went wrong'
    })
  })
};

contractorMaster.deleteContractor = (req, res)=> {
  var reqObj = {};
  reqObj['id'] = req.query.id;
  userService.findAndDelete(reqObj).then(resp=> {
      res.send({
          status: true,
          msg: 'Deleted Successfully'
      });
  }, err=> {
      res.status(404).send({
          status: false,
          data: {},
          msg: err
      });
  }).catch(err=> {
 console.log("err ", err);
    res.status(500).send({
      status: false,
      data: {},
      msg: 'Something went wrong'
    })
  })
};

module.exports = contractorMaster;