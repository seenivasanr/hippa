var patientService = require('../services/patientService');
var swapkitService = require('../services/swapService.js');
var dashboardMaster = {};

dashboardMaster.dashboardView = (req, res)=> {
    var reqObj = {};
    if(req.session.user.role == 'contractor') {
        reqObj['agentId'] = req.session.user.id;
    }
    patientService.findAndGroup(reqObj).then(patient=> {
        var data = {};
        patient.forEach(element => {
            data[element.testStatus] = element.statusCount;
        });
        swapkitService.findAndGroup().then(swap=> {
            swap.forEach(element => {
                data[element.orderStatus] = element.statusCount;
            });
            console.log("data ", data);
            res.render('pages/dashboard', {
                data: data
            });
        })
    }).catch(err=> {
        res.render('pages/dashboard',{
            data: {}
        });
    })
};

module.exports = dashboardMaster;