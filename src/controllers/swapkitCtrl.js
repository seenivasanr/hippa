var swapkitService = require('../services/swapService.js');

var swapMaster = {};

swapMaster.swapList = (req, res)=> {
    var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['patientName'] = `%${req.query['patientName']? req.query['patientName']: ''}%`;
    reqObj['contractorName'] = `%${req.query['contractorName']? req.query['contractorName'].trim(): ''}%`;
    reqObj['orderStatus'] = req.query['orderStatus']? req.query['orderStatus']: '%%';
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    console.log("reqObj ", reqObj);
    swapkitService.findAll(reqObj, pagination).then(resp=> {
        res.render('pages/swapkit/list', {
            data: resp.rows,
            current: pagination['page'],
            pages: Math.ceil(resp.count / pagination['perPage']),
            perPage: pagination['perPage'],
            patientName : req.query['patientName'],
            contractorName : req.query['contractorName'],
            orderStatus : req.query['orderStatus'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
        });
    }, err=> {
        res.render('pages/swapkit/list', {
            data: [],
            current: 1,
            pages: 1,
            patientName : req.query['patientName'],
            contractorName : req.query['contractorName'],
            orderStatus : req.query['orderStatus'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
        });
    }).catch(err=> {
        // re
    })
};

swapMaster.swapKitView = (req, res) => {
    console.log("req.params ", req.params);
    swapkitService.findOne(req.params.id).then(resp=> {
        console.log("resp ==>swapKitView", resp);
        res.render('pages/swapkit/view', {
            data: resp,
            params: req.params
        })
    },err=> {
        res.redirect(400, '/swapkit/list/1');
    }).catch(err=> {
        res.redirect(500, '/swapkit/list/1');
    })
        
};

swapMaster.swapKitEdit = (req, res) => {
    console.log("req.params ", req.params);
    swapkitService.findOne(req.params.id).then(resp=> {
        console.log("resp ", resp);
        res.render('pages/swapkit/edit', {
            data: resp,
            params: req.params
        })
    },err=> {
        res.redirect(400, '/swapkit/list/1');
    }).catch(err=> {
        res.redirect(500, '/swapkit/list/1');
    })
        
};

swapMaster.updateSwapkit = (req, res) => {
    swapkitService.findAndUpdate(req.body).then(resp=> {
        res.send({
            status: true,
            data: resp,
            msg: 'Updated Successfully'
        })
    }, err=> {
        res.status(400).send({
            status: false,
            data: {},
            msg: 'Invalid Request'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    });
};

swapMaster.deleteSwapkit = (req, res)=> {
    var reqObj = {};
    reqObj['id'] = req.query.id;
    swapkitService.findAndDelete(reqObj).then(resp=> {
        res.send({
            status: true,
            msg: 'Deleted Successfully'
        });
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: err
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

module.exports = swapMaster;