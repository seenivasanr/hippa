var userService = require('../services/userService.js');
const config = require("../config/config")[process.env.NODE_ENV || "development"];

var common = require('../common/common.js');
var states = require('../common/states');

var jwt = require('jsonwebtoken');
// const jwt = require("jwt-simple");
const ejs = require("ejs");
const fs = require("fs");
const randomstring = require("randomstring");
const { check, validationResult, matchedData } = require("express-validator");

var userMaster = {};

userMaster.login = function(req,res) {
	var user = req.body;
	var result = {};
	userService.findOneAndAuth({email: user.username, password: user.password}).then(resp => {
		result = resp;
		res.send({
			status : true,
			data : result,
		});
	}).catch(err=> {
		res.send({
			status : false,
			msg : "Invalid Username/Password"
		});
	});
};

userMaster.sendResetLink = (req, res) => {
	var result = {};
	userService.findOne(req.body).then(async(resp)=> {
		var user = resp;
		console.log("resp ===>sendResetLink", resp);
		var payload = {};
		payload['id'] = user.id;
		payload['email'] = user.email;
		
		var token = jwt.sign(payload, config.secret_key, { expiresIn: '1h' });
		var url = `${config.host}/api/v1/reset/${payload['id']}/${token}`;
		var filePath = 'views/pages/template/reset.ejs';
		var compiled = ejs.compile(fs.readFileSync(filePath, 'utf8'));
		var mailObj = {};
		mailObj['to'] = payload['email'];
		mailObj['subject'] = 'Genetix HIPPA - Reset Password';
		mailObj['html'] = compiled({ link: url });
		common.sendMail(mailObj).then(resp=> {
			req.toastr.success('Email Send To Registered Mail Id', "Success");
			res.redirect('/login');
			// res.send({
			// 	status: true,
			// 	data: user,
			// 	msg: 'Mail send successfully'
			// });
		}, err=> {
			res.status(417).send({
				status: false,
				data: {},
				msg: 'Failed to send a mail'
			})
		})
	}, err=> {
		req.toastr.error('No User Found.', "Error");
		res.redirect(404, '/forgotpassword');
	}).catch(err=> {
		res.status(500).send({
			status: false,
			data: {},
			msg: 'Internal server error'
		})
	})
};

userMaster.redirectPassword = (req, res) => {
	// var secret = 'fe1a1915a379f3be5394b64d14794932-1506868106675';
	jwt.verify(req.params.token, config.secret_key, function(err, decoded) {
		if (err) {
			res.status(401).send({
				status: false,
				data: {},
				msg: 'Invalid Token'
			});
		} else {
			var url = req.protocol + '://' + req.get('host');
			res.render('pages/auth/reset_password', {data: req.params, url: url});
		}
	});
	// var payload = jwt.decode(req.params.token, config.secret_key);
	// if (payload) {
	// 	// res.redirect('/reset', {req: req.params});
	// 	var url = req.protocol + '://' + req.get('host');
	// 	res.render('pages/auth/reset_password', {data: req.params, url: url});
	// } else {
	// 	res.status(401).send({
	// 		status: false,
	// 		data: {},
	// 		msg: 'Unauthorized'
	// 	});
	// }
};

userMaster.resetPassword = (req, res) => {
	console.log('reset password', req.body);
	var data = req.body;
	common.genHash(req.body.password).then(secret=> {
		data['secret'] = secret;
		userService.findOneAndUpdate(data).then(resp=> {
			req.toastr.success('Password Changed Successfully', 'Success')
			res.redirect('/login');
		},err=> {
			req.toastr.error('No User Found', 'Error');
			res.redirect(404, '/login')
		}).catch(err=> {
			req.toastr.error('Some went wrong on password update', 'Error');
			res.redirect(404, '/login')
		})
	})
}

userMaster.changePassword = (req, res) => {
	console.log('change password', req.body);
	const errors = validationResult(req);
	console.log("errors ", errors.mapped());
	var data = req.body;
	if (!errors.isEmpty()) {
		req.app.locals.obj = {
			data: req.body,
			errors: errors.mapped()
		}
		res.redirect('/profile/changePassword');
	} else {
		data['id'] = req.session.user.id;
		userService.findAndValidate(data).then(resp=> {
			common.genHash(data.newPassword).then(secret=> {
				data['secret'] = secret;
				userService.findOneAndUpdate(data).then(resp=> {
					req.toastr.success('Password Changed Successfully', 'Success')
					res.redirect('/profile');
				},err=> {
					req.toastr.error('No User Found', 'Error');
					res.redirect(404, '/profile')
				}).catch(err=> {
					req.toastr.error('Some went wrong on password update', 'Error');
					res.redirect(500, '/profile')
				})
			})
		}, err=> {
			if (err == 'Oops! Wrong password.') {
				var errorsMsg = { oldPassword: { msg: 'Old Password is Wrong'} };
			}
			req.app.locals.obj = {
				data: req.body,
				errors: errorsMsg? errorsMsg: {}
			}
			res.redirect('/profile/changePassword');
			// res.render("pages/profile/changePassword", {
			// 	data: req.body,
			// 	errors: errors.mapped()
			// });
		}).catch(err=> {
			req.toastr.error('Some went wrong on password update', 'Error');
			res.redirect(500, '/login')
		})
	}
}

userMaster.getUserProfile = (req, res) => {
	reqObj = {};
	reqObj['id'] = req.session.user.id;
	userService.findOne(reqObj).then(resp=> {
		var user = resp;
		res.render('pages/profile/profile_view',{user: user})
	}, err=> {
 		console.log("err ", err);

	});
};

userMaster.editProfileView = (req, res)=> {
	reqObj = {};
	reqObj['id'] = req.session.user.id;
	userService.findOne(reqObj).then(resp=> {
		var user = resp;
		res.render('pages/profile/profile_edit',{
			user: user,
			states: states
		})
	}, err=> {
 		console.log("err ", err);

	});
}

userMaster.profileUpload = (req, res)=> {
	// if (req.files && req.files.image) {
 	// 	console.log("req.files.image ", req.files.image);
		var data = {};
		data['id'] = req.session.user.id;
		data['image_url'] = req.files ? req.files.image.path: null;
		userService.findOneAndUpdateProfile(data).then(resp=> {
			req.session.user.image_url = resp.image_url;
			res.send({
				status: true,
				data: resp,
				msg: 'Image Upload Successfully'
			});
		}, err=> {
			res.status(400).send({
				status: false,
				data: {},
				msg: 'Failed to Update Image'
			});
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wroung'
			});
		});
	// }
};

userMaster.updateProfile = (req, res)=> {
	var data = req.body;
	data['id'] = req.session.user.id;
	userService.findOneAndUpdateProfile(data).then(resp=> {
		req.session.user.name = resp.fullName;
		req.toastr.success('Profile Updated Successfully', 'Success')
		res.redirect('/profile');
	}, err=> {
		if (err == 'No Records Found') {
			req.toastr.error('No User Found', 'Error');
			res.redirect(404, '/profile')
		} else {
			req.toastr.error('Bad Request', 'Error');
			res.redirect(400, '/profile')
		}
	}).catch(err=> {
		res.status(500).send({
			status: false,
			data: {},
			msg: 'Something went wroung profile update'
		});
	})
};

userMaster.checkUserName = (req, res)=> {
    userService.findUserName(req.body).then(resp=> {
        console.log("resp ", resp);
        res.send({
            status: true,
            data: resp,
            msg: 'User name is already exists'
        });
    }, err=> {
        res.send({
            status: false,
            data: {},
            msg: err
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
}

module.exports = userMaster;