var labService = require('../services/labService');

var states = require('../common/states');

var labMaster = {};

labMaster.createLab = (req, res) => {
    labService.findAndCreate(req.body).then(resp=> {
        req.toastr.success("Created Successfully", "Success");
        res.redirect('/lab/list/1');
    }, err=> {
        res.render('pages/lab/add', {
            data: req.body,
            states: states
        });
    }).catch(err=> {
        res.render('pages/lab/add', {
            data: req.body,
            states: states
        });
    })
};

labMaster.labList = (req, res) => {
    var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['labName'] = `%${req.query['labName']? req.query['labName']: ''}%`;
    reqObj['email'] = `%${req.query['email']? req.query['email']: ''}%`;
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    console.log("reqObj ", reqObj);
    labService.findAll(reqObj, pagination).then(resp=> {
        console.log("resp ", resp);
        res.render('pages/lab/list', {
            data: resp.rows,
            current: pagination['page'],
            pages: Math.ceil(resp.count / pagination['perPage']),
            perPage: pagination['perPage'],
            labName : req.query['labName'],
            email : req.query['email'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
        });
    }, err=> {
        res.render('pages/lab/list', {
            data: [],
            current: 1,
            pages: 1,
            labName : req.query['labName'],
            email : req.query['email'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
        });
    }).catch(err=> {
        // re
    })
};

labMaster.editView = (req, res) => {
    console.log("req.params ", req.params);
    labService.findById(req.params.id).then(resp=> {
        console.log("resp ", resp);
        res.render('pages/lab/edit', {
            data: resp,
            states: states,
            params: req.params
        })
    },err=> {
        res.redirect(400, '/lab/list/1');
    }).catch(err=> {
        res.redirect(500, '/lab/list/1');
    })
};

labMaster.updateLab = (req, res)=> {
    labService.findAndUpdate(req.body).then(resp=> {
        res.send({
            status: true,
            data:resp,
            msg: "Updated Successfully"
        })
    }, err=> {
        res.status(400).send({
            status: false,
            data: {},
            msg: 'Invalid Request'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

labMaster.labView = (req, res) => {
    console.log("req.params ", req.params);
    labService.findById(req.params.id).then(resp=> {
        console.log("resp ", resp);
        res.render('pages/lab/view', {
            data: resp,
            states: states,
            params: req.params
        })
    },err=> {
        res.redirect(400, '/lab/list/1');
    }).catch(err=> {
        res.redirect(500, '/lab/list/1');
    })
        
};

labMaster.deleteLab = (req, res)=> {
    var reqObj = {};
    reqObj['id'] = req.query.id;
    labService.findAndDelete(reqObj).then(resp=> {
        res.send({
            status: true,
            msg: 'Deleted Successfully'
        });
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: err
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

module.exports = labMaster;