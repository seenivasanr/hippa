var formService = require('../services/formService');
var formMaster = {};

formMaster.formList = (req, res) => {
    formService.findAll().then(resp=> {
        res.render('pages/forms/list',{
            data: resp
        })
    })
}

formMaster.getFormById = (req, res) => {
    var result = {};
    formService.findOneTest(req.params).then(resp=> {
        result = Object.assign({}, resp);
        result.formFields = JSON.parse(resp.formFields);
        res.render('pages/forms/add_form', {
          data: result
        })
    }, err=> {
        res.redirect('/form/list')
    })
}

formMaster.updateForm = (req, res) => {
    var reqObj = {};
    reqObj['id'] = req.body.id;
    reqObj['updateData'] = {formFields: JSON.stringify(req.body.dynamicForm)};
    formService.findAndUpdate(reqObj).then(resp=> {
        // req.toastr.success("Updated Successfully", "Success");
        // res.redirect('/form/list');
        res.send({
            status: true,
            msg: 'Updated Successfully',
            data: {}
        });
    }, err=> {
        res.render(404,`/form/add/${req.body.id}`, {
            data: req.body
        });
    })
}

module.exports = formMaster;