var userService = require('../services/userService');

var common = require("../common/common.js");
const config = require('../config/config')[process.env.NODE_ENV || "development"];
var states = require('../common/states');

const ejs = require("ejs");
const fs = require("fs");
const randomstring = require("randomstring");

var employeeMaster = {};

employeeMaster.addEmployee = (req, res)=> {
    var data = req.body;
    data['role'] = 'employee';
    data['isActive'] = data.status;
    !data['dob'] && delete data['dob'];
    var random = randomstring.generate(6);
    common.genHash(random).then(secret => {
        data["password"] = secret;
        userService.findAndCreate(data).then(resp => {
            var filePath = "views/pages/template/password.ejs";
            var compiled = ejs.compile(fs.readFileSync(filePath, "utf8"));
            var mailObj = {};
            mailObj["to"] = data["email"];
            mailObj["subject"] = "Genetix HIPAA - Welcome";
            mailObj["html"] = compiled({data: data, random: random, baseUrl: config.host});
            common.sendMail(mailObj);
            req.toastr.success("Created Successfully", "Success");
            res.redirect("/employee/list/1");
        }, err => {
            req.toastr.error("Username Already Exists", "Error");
            req.app.locals.obj = req.body;
            res.redirect("/employee/add");
        });
    });
};

employeeMaster.employeeList = (req, res) => {
	var reqObj = {};
	var pagination = {};
	pagination['perPage'] = 10;
	pagination['page'] = req.params.page || 1
	reqObj['role'] = 'employee';
	reqObj['fullName'] = `%${req.query['fullName']? req.query['fullName']: ''}%`;
    reqObj['email'] = `%${req.query['email']? req.query['email']: ''}%`;
	reqObj['state'] = `%${req.query['state']? req.query['state']: ''}%`;
	reqObj['groupPracticeName'] = `%${req.query['groupPracticeName']? req.query['groupPracticeName']: ''}%`;

    var status = '';
    if (req.query['status']) {
        status = req.query['status'] == 'true' ? true : false;
        reqObj['status'] = status;
    } else {
        reqObj['status'] = '%%';
    }
	reqObj['permission'] = `%${req.query['permission']? req.query['permission']: ''}%`;
	reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
	userService.findByRole(reqObj, pagination).then(resp=> {
		//  console.log("resp ", resp);
		res.render('pages/employee/list', {
			data: resp.rows,
			current: pagination['page'],
			pages: Math.ceil(resp.count / pagination['perPage']),
            perPage: pagination['perPage'],
            fullName : req.query['fullName'],
            email : req.query['email'],
            status : req.query['status'],
            permission : req.query['permission'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
        });
	}, err=> {
		res.render('pages/employee/list', {
			data: [],
			current: 1,
            pages: 1,
            fullName : req.query['fullName'],
            email : req.query['email'],
            status : req.query['status'],
            permission : req.query['permission'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
		})
	})
};

employeeMaster.viewEmployee = (req, res)=> {
    userService.findOne({id: req.params.id}).then(resp=> {
        res.render('pages/employee/view',{
          data: resp
        })
    }, err=> {
        res.redirect(404,'/employee/list/1');
    }).catch(err=> {
        res.redirect(500,'/employee/list/1');
    })
};

employeeMaster.EditView = (req, res)=> {
    userService.findOne({id: req.params.id}).then(resp=> {
        res.render('pages/employee/edit',{
          data: resp,
          params: req.params,
          states: states
        })
    }, err=> {
        res.redirect(404,'/employee/list/1');
    }).catch(err=> {
        res.redirect(500,'/employee/list/1');
    })
};

employeeMaster.updateEmployee = (req, res)=> {
    var data = req.body;
    data['isActive'] = data.status;
    userService.findOneAndUpdateProfile(req.body).then(resp=> {
        res.send({
          status: true,
          data:resp,
          msg: "Updated Successfully"
        });
    }, err=> {
        res.status(400).send({
            status: false,
            data: {},
            msg: 'Invalid Request'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        });
    })
};

employeeMaster.deleteEmployee = (req, res)=> {
    var reqObj = {};
    reqObj['id'] = req.query.id;
    userService.findAndDelete(reqObj).then(resp=> {
        res.send({
            status: true,
            msg: 'Deleted Successfully'
        });
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Record Found'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

module.exports = employeeMaster;