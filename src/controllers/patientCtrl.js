var formService = require("../services/formService");
var patientService = require("../services/patientService");
var userService = require("../services/userService");
var physicianService = require('../services/physicianService');
var labService = require('../services/labService');
var swapService = require('../services/swapService');
var swapkitApiService = require('../services/swapkitApiService');

var common = require("../common/common.js");
var states = require('../common/states');
var config = require('../config/config.js')[process.env.NODE_ENV || "development"];

var jwt = require('jsonwebtoken');
const ejs = require("ejs");
const fs = require("fs");
var moment = require('moment');
// let pdf = require("html-pdf");


var patientMaster = {};

patientMaster.formList = (req, res) => {
    formService.findAll().then(resp=> {
        res.render('pages/patient/form_list',{
            data: resp
        })
    })
}

patientMaster.createPatient = (req, res) => {
    var data = req.body;
    // if (req.session.user.role != 'employee') {
        data['agentId'] = req.session.user.id;
    // }
    data['createrId'] = req.session.user.id;
    data['createdBy'] = req.session.user.role;
    data['diagnosedWithCancer'] = JSON.stringify(data.diagnosedWithCancer);
    data['ashkenaziJewishAncestry'] = JSON.stringify(data.ashkenaziJewishAncestry);
    data['colonPolyps'] = JSON.stringify(data.colonPolyps);
    data['dnaCancerScreening'] = JSON.stringify(data.dnaCancerScreening);
    data['familydetails'] = JSON.stringify(data.familydetails);
    data['dob'] = new Date(data.dob);
    console.log("req.body ====>createPatient", data);
    console.log("req.session ", req.session.user);
    patientService.findAndCreate(data).then(resp=> {
        console.log("resp ===>createPatient", resp);
        res.send({
            status: true,
            data: resp,
            msg: 'Added Successfully'
        });
    }, err=> {
        res.status(400).send({
            status: false,
            data: {},
            msg: 'Email Id Already Exists'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        });
    })
};

patientMaster.updatePatient = (req, res) => {
    var data = JSON.parse(req.body.data);
    if (req.files && req.files.image) {
        data.signatureFile = req.files.image.path;
        data.signatureFileName = req.files.image.originalFilename;
    }
    // return false
    data['diagnosedWithCancer'] = JSON.stringify(data.diagnosedWithCancer);
    data['ashkenaziJewishAncestry'] = JSON.stringify(data.ashkenaziJewishAncestry);
    data['colonPolyps'] = JSON.stringify(data.colonPolyps);
    data['dnaCancerScreening'] = JSON.stringify(data.dnaCancerScreening);
    data['familydetails'] = JSON.stringify(data.familydetails);
    data['dob'] = new Date(data.dob);
    !data['sign_date'] && delete data['sign_date'];
    console.log("req.body ====>updatePatient", data);
    patientService.findAndUpdate(data).then(resp=> {
        var patient = resp;
        console.log("patient ==>", patient);
        if (resp.testStatus == 'Doctor Approved') {
            swapService.findById({patientId: resp.id, orderStatus: 'ordered'}).then(resp=> {
                console.log("resp ", resp);
                if(resp) {
                    res.send({
                        status: true,
                        data: resp,
                        msg: 'Updated Successfully'
                    });
                } else {
                    console.log("swapkit record is not for this user");
                    var swapReqObj = {};
                    // swapReqObj['clientID'] = config.swapkit.clientID;
                    swapReqObj['clientID'] = patient.agent.constractorId;
                    swapReqObj['country'] = config.swapkit.country;
                    swapReqObj['outboundShippingServiceLevel'] = config.swapkit.outboundShippingServiceLevel;
                    swapReqObj['items'] = config.swapkit.items;
                    swapReqObj['shipTo'] = patient.swapkitshipedTo;
                    swapReqObj['patients'] = [];
                    swapReqObj['patients'][0] = {};
                    swapReqObj['patients'][0].facilityID = "1";
                    swapReqObj['patients'][0].patientName = `${patient.firstName } ${patient.lastName}`;
                    swapReqObj['patients'][0].patientID = patient.id;
                    var date = new Date(patient.dob);
                    //swapReqObj['patients'][0].patientDOB = date.getDate()+'-' + (date.getMonth()+1) + '-'+date.getFullYear();
                    swapReqObj['patients'][0].additionalID = "1";
                    if (patient.swapkitshipedTo == 'home') {
                        swapReqObj['address1'] = patient.address;
                        swapReqObj['city'] = patient.city;
                        swapReqObj['state'] = patient.state;
                        swapReqObj['zip'] = patient.zipcode;
                        swapReqObj['phone'] = patient.phoneNo;
                    } else if(patient.swapkitshipedTo == 'physician') {
                        swapReqObj['address1'] = patient.physician.address;
                        swapReqObj['city'] = patient.physician.city;
                        swapReqObj['state'] = patient.physician.state;
                        swapReqObj['zip'] = patient.physician.zipcode;
                        swapReqObj['phone'] = patient.physician.phoneNo;
                    } else {
                        swapReqObj['address1'] = patient.agent.streetAddress;
                        swapReqObj['city'] = patient.agent.city;
                        swapReqObj['state'] = patient.agent.state;
                        swapReqObj['zip'] = patient.agent.zipcode;
                        swapReqObj['phone'] = patient.agent.phoneNo;
                    }
                    console.log("swapReqObj ", swapReqObj);
                    swapkitApiService.orderSave(swapReqObj).then(resp=> {
                        console.log("resp ==>orderSave", resp);
                        var saveObj = {};
                        saveObj['orderNo'] = resp.orderNumber;
                        saveObj['orderStatus'] = 'ordered';
                        saveObj['orderedBy'] = 'employee';
                        saveObj['shipTo'] = patient.swapkitshipedTo;
                        saveObj['orderCopy'] = JSON.stringify(swapReqObj);
                        saveObj['userId'] = req.session.user.id;
                        saveObj['orderDate'] = new Date();
                        saveObj['patientId'] = patient.id;
                        saveObj['agentId'] = patient.agentId;
                        console.log("saveObj ", saveObj);
                        swapService.findAndCreate(saveObj).then(resp=> {
                            res.send({
                                status: true,
                                data: resp,
                                msg: 'Updated Successfully'
                            });
                        }).catch(err=> {
                            res.status(500).send({
                                status: false,
                                data: {},
                                msg: 'Something went wrong'
                            })
                        })
                    }).catch(err=> {
                        data['testStatus'] = 'Dr.Approval Pending';
                        data['doctorApproved'] = null;
                        patientService.findAndUpdate(data);
                        res.status(500).send({
                            status: false,
                            data: {},
                            msg: err
                        })
                    })
                }
            }).catch(err=> {
                console.log("err ", err);
                res.status(500).send({
                    status: false,
                    data: {},
                    msg: 'Something went wrong'
                })
            })
        } else {
            res.send({
                status: true,
                data: resp,
                msg: 'Updated Successfully'
            });
        }
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Records Found'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        });
    })
}

patientMaster.uploadFile = (req, res)=> {
    if (req.files && req.files.image) {
        res.send({
            status: true,
            msg: "Upload successfully",
            data: req.files.image
        });
    }
}

patientMaster.renderPatientForm = (req, res)=> {
    reqObj = {};
    reqObj['id'] = req.session.user.id;
    userService.findOne(reqObj).then(resp=> {
        var userInfo = resp;
        physicianService.findAllList({}).then(resp=> {
            var physicians = resp;
            // console.log("resp ==>physicianService", resp);
            labService.findAllList().then(labs=> {
                // console.log("resp ==>labService", labs);
                userService.findAllContractorList().then(contractor=> {
                    res.render('pages/patient/add', {
                        agent: userInfo,
                        physicianList: physicians,
                        labList: labs,
                        contractorList: contractor,
                        testName: req.params.testName ? req.params.testName: '',
                        states: states
                    })
                })
            })
        })
        // res.render('pages/profile/profile_view',{user: user})
    }, err=> {
        console.log("err ", err);

    });
}

patientMaster.UpdatePatientandUserInfo = (req, res)=> {
    var data = req.body;
    delete data['physicians'];
    data['ethnicity'] = JSON.stringify(data.ethnicity);
    data['preInsuranceType'] = data.insuranceInfo[0].insuranceName == 'Primary Insurance' ? data.insuranceInfo[0].insuranceType: data.insuranceInfo[1].insuranceType;
    data['prepolicy'] = data.insuranceInfo[0].insuranceName == 'Primary Insurance' ? data.insuranceInfo[0].policy: data.insuranceInfo[1].policy;
    if (data.insuranceInfo[0].insuranceName == 'Secondary Insurance' || (data.insuranceInfo[1] && data.insuranceInfo[1].insuranceName == 'Secondary Insurance')) {
        var secInsuranceData = data.insuranceInfo[1].insuranceName == 'Secondary Insurance' ? data.insuranceInfo[1] : data.insuranceInfo[0];
        data['secInsuranceType'] = secInsuranceData.insuranceType
        data['secpolicy'] = secInsuranceData.policy
    }
    
    data['insuranceInfo'] = JSON.stringify(data.insuranceInfo);
    data['cardImages'] = JSON.stringify(data.cardImages);
    data['physicianIds'] = JSON.stringify(data.physicianIds);
    // data['physicianId'] = data.physicianId;
    data['dob'] = new Date(data.dob);
    console.log("req.body ===>UpdatePatientandUserInfo", data);
    patientService.findAndUpdate(data).then(resp=> {
        res.send({
            status: true,
            data: resp,
            msg: 'Updated Successfully'
        });
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Records Found'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        });
    })
};

patientMaster.updatedThirdForm = (req, res)=> {
    var data = req.body;
    if (data.remoteSignature == true) {
        data.sign_token = null;
    }
    if (data['cardImages']) {
        data['cardImages'] = JSON.stringify(data.cardImages);
    }
    console.log("data ===>updatedThirdForm", data);
    patientService.findAndUpdate(data).then(async(resp)=> {
        var patient = resp;
        // physicianService.findById(patient.physicianId).then(async(resp)=>{
            patient['ethnicity'] = JSON.parse(patient.ethnicity);
            patient['insuranceInfo'] = JSON.parse(patient.insuranceInfo);
            patient['familydetails'] = JSON.parse(patient.familydetails);
            // patient['physician'] = resp;
            console.log("patient ", patient);
            // if (data.state == 'Edit') {
                res.send({
                    status: true,
                    data: resp,
                    msg: 'Updated Successfully'
                });
                return false;
            // }
            // console.log("data.state ", data.state);
            // var filePath = "views/pages/template/patientPdf.ejs";
            // var compiled =  await ejs.compile(fs.readFileSync(filePath, "utf8"));
            // var mailObj = {};
            // // mailObj["to"] = patient["email"];
            // mailObj["to"] = patient['physician'].email;
            // console.log("mailObj[] ", mailObj["to"]);
            // mailObj["subject"] = "Genetix HIPAA - Welcome";
            // mailObj["html"] = await compiled({data: patient, moment: moment});
            // mailObj["filename"] = `${new Date().getTime()}-${patient.id}.pdf`;
            // // console.log("mailObj ", mailObj);
            // common.createPdf(mailObj["html"], mailObj["filename"]).then(resp=> {
            //     mailObj['filepath'] = resp.filename;
            //     console.log("mailObj['filepath'] ", mailObj['filepath']);
            //     common.sendMail(mailObj);
            //     res.send({
            //         status: true,
            //         data: resp,
            //         msg: 'Updated Successfully'
            //     });
            // })
        // }, err=> {
        //     res.status(404).send({
        //         status: false,
        //         data: {},
        //         msg: 'No Records Found'
        //     });
        // }).catch(err=> {
        //     res.status(500).send({
        //         status: false,
        //         data: {},
        //         msg: 'Something went wrong'
        //     })
        // })
    }, err=> {
 console.log("err ", err);
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Records Found'
        });
    }).catch(err=> {
        console.log("err ", err);
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        });
    })
};

patientMaster.checkSignatureAndUpdate = (req, res)=> {
    var data = req.body;
    if (data.remoteSignature == true) {
        data.sign_token = null;
    }
    console.log("data ===>checkSignatureAndUpdate", data);
    patientService.findById(data.id).then(resp=> {
        if (!data.signature && !resp.signature) {
            res.status(400).send({
                status: false,
                data: {},
                msg: 'Singature is required'
            });
            return false;
        }
        patientService.findAndUpdate(data).then(async(resp)=> {
            var patient = resp;
            patient['ethnicity'] = JSON.parse(patient.ethnicity);
            patient['insuranceInfo'] = JSON.parse(patient.insuranceInfo);
            patient['familydetails'] = JSON.parse(patient.familydetails);
            console.log("patient ", patient);
            res.send({
                status: true,
                data: resp,
                msg: 'Updated Successfully'
            });
            return false;
        }, err=> {
            res.status(404).send({
                status: false,
                data: {},
                msg: 'No Records Found'
            });
        }).catch(err=> {
            console.log("err ", err);
            res.status(500).send({
                status: false,
                data: {},
                msg: 'Something went wrong'
            });
        })
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Records Found'
        });
    }).catch(err=> {
        console.log("err ", err);
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        });
    })
};

patientMaster.getPatienList = (req, res)=> {
    var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
 console.log("req ", req._parsedOriginalUrl.search);
    
    var data = {};
    if(req.session.user.role == 'contractor') {
        data['agentId'] = req.session.user.id;
    } else {
        data['createrId'] = req.session.user.id;
    }
    
    data['role'] = req.session.user.role;
    data['notPaidStatus'] = true;
    
    if (data['role'] == 'admin') {
        data['name'] = "%%";
        data['contractorName'] = `%${req.query['name']? req.query['name'].trim(): ''}%`;
    } else {
        data['name'] = `%${req.query['name']? req.query['name'].trim(): ''}%`;
        data['contractorName'] = "%%";
    }
    // data['name'] = `%${req.query['name']? req.query['name'].trim(): ''}%`;
    data['testStatus'] = req.query['testStatus']? req.query['testStatus']: '%%';
    data['notPaidStatus'] = req.query['testStatus']? false: true;
    data['formStatus'] = req.query['formStatus']?req.query['formStatus']: `Complete`;
    // data['gender'] = `%${req.query['gender']? req.query['gender']: ''}%`;
    data['submissionDate'] = req.query['submissionDate'];
    data['dob'] = req.query['dob'];
    data['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [[ 'createdAt', 'ASC' ]];
    console.log("data ", data);
    patientService.findAll(data, pagination).then(resp=> {
        res.render('pages/patient/list', {
            data: resp.rows,
            current: pagination['page'],
            pages: Math.ceil(resp.count / pagination['perPage']),
            perPage: pagination['perPage'],
            name: req.query['name']? req.query['name']:'',
            testStatus: req.query['testStatus']? req.query['testStatus']: '',
            formStatus: req.query['formStatus']? req.query['formStatus']: '',
            submissionDate: req.query['submissionDate']? req.query['submissionDate']: '',
            dob: req.query['dob']? req.query['dob']: '',
            gender: req.query['gender']? req.query['gender']: '',
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
        });
    }, err=> {
        res.render('pages/patient/list', {
            data: [],
            current: 1,
            pages: 1,
            name: req.query['name']? req.query['name']:'',
            testStatus: req.query['testStatus']? req.query['testStatus']: '',
            formStatus: req.query['formStatus']? req.query['formStatus']: '',
            submissionDate: req.query['submissionDate']? req.query['submissionDate']: '',
            dob: req.query['dob']? req.query['dob']: '',
            gender: req.query['gender']? req.query['gender']: '',
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
        });
    }).catch(err=> {

    })
};

patientMaster.viewPatient = (req, res)=> {
    var reqObj = {};
    reqObj['id'] = req.session.user.id;
    userService.findOne(reqObj).then(resp=> {
        var agentInfo = resp;
        patientService.findById(req.params.id).then(resp=> {
            var patient = resp;
            patient['familydetails'] = JSON.parse(resp['familydetails']);
            patient['diagnosedWithCancer'] = JSON.parse(resp['diagnosedWithCancer']);
            patient['ashkenaziJewishAncestry'] = JSON.parse(resp['ashkenaziJewishAncestry']);
            patient['colonPolyps'] = JSON.parse(resp['colonPolyps']);
            patient['dnaCancerScreening'] = JSON.parse(resp['dnaCancerScreening']);
            patient['ethnicity'] = JSON.parse(resp['ethnicity']);
            patient['insuranceInfo'] = JSON.parse(resp['insuranceInfo']);
            patient['cardImages'] = JSON.parse(resp['cardImages']);
            patient['physicianIds'] = resp['physicianIds'] !=null ? JSON.parse(resp['physicianIds']): [];
            console.log("patient ==>", patient);
            physicianService.findMultipleIds(patient['physicianIds']).then(resp=> {
                res.render('pages/patient/view', {
                    patient: patient,
                    agent: agentInfo,
                    physicians: resp
                });
            })
        })
    })
};

patientMaster.deletePatient = (req, res)=> {
    var reqObj = {};
    // reqObj['DeletedAt'] = new Date();
    reqObj['id'] = req.query.id;
    patientService.findAndDelete(reqObj).then(resp=> {
        console.log("resp ==>deletePatient", resp);
        res.send({
            status: true,
            msg: 'Deleted Successfully'
        });
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Patient Found'
        });
    })
};

patientMaster.downloadPDF = (req, res)=> {
    patientService.findById(req.params.id).then(async(resp)=> {
        var patient = resp;
        // physicianService.findById(patient.physicianId).then(async(resp)=>{
            patient['ethnicity'] = JSON.parse(patient.ethnicity);
            patient['insuranceInfo'] = JSON.parse(patient.insuranceInfo);
            patient['familydetails'] = JSON.parse(patient.familydetails);
            // patient['physician'] = resp;
            var filePath = "views/pages/template/patientPdf.ejs";
            var compiled =  await ejs.compile(fs.readFileSync(filePath, "utf8"));
            var pdfObj = {};
            pdfObj["html"] = await compiled({data: patient, moment: moment});
            pdfObj["filename"] = `${new Date().getTime()}-${patient.id}.pdf`;
            common.createPdf(pdfObj["html"], pdfObj["filename"]).then(resp=> {
                pdfObj['filepath'] = resp.filename;
                console.log("pdfObj['filepath'] ", pdfObj['filepath']);
                res.download(pdfObj['filepath'], function (err) {
                    if (err) {
                        console.log("Error");
                        console.log(err);
                    } else {
                        console.log("Success");
                    }    
                });
            })
        // }, err=> {
        //     res.status(404).send({
        //         status: false,
        //         data: {},
        //         msg: 'No Records Found'
        //     });
        // }).catch(err=> {
        //     res.status(500).send({
        //         status: false,
        //         data: {},
        //         msg: 'Something went wrong'
        //     })
        // })
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Records Found'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

patientMaster.editForm = (req, res)=> {
    var reqObj = {};
    reqObj['id'] = req.session.user.id;
    userService.findOne(reqObj).then(resp=> {
        var agentInfo = resp;
        patientService.findById(req.params.id).then(resp=> {
            var patient = resp;
            patient['familydetails'] = JSON.parse(resp['familydetails']);
            patient['diagnosedWithCancer'] = JSON.parse(resp['diagnosedWithCancer']);
            patient['ashkenaziJewishAncestry'] = JSON.parse(resp['ashkenaziJewishAncestry']);
            patient['colonPolyps'] = JSON.parse(resp['colonPolyps']);
            patient['dnaCancerScreening'] = JSON.parse(resp['dnaCancerScreening']);
            patient['ethnicity'] = JSON.parse(resp['ethnicity']);
            patient['insuranceInfo'] = JSON.parse(resp['insuranceInfo']);
            patient['cardImages'] = JSON.parse(resp['cardImages']);
            console.log("resp['physicianIds'] ", resp['physicianIds']);
            patient['physicianIds'] = resp['physicianIds'] !=null ? JSON.parse(resp['physicianIds']): [];
            console.log("patient['physicianIds']  ", patient['physicianIds'] );
            physicianService.findMultipleIds(patient.physicianIds).then(resp=> {
                patient['physicians'] = resp;
                console.log("patient ==>", patient);
                physicianService.findAllList({}).then(resp=> {
                    var physicianList = resp;
                    // console.log("resp ==>physicianService", resp);
                    labService.findAllList().then(labs=> {
                        userService.findAllContractorList().then(contractor=> {
                            res.render('pages/patient/edit', {
                                patient: patient,
                                agent: agentInfo,
                                physicianList: physicianList,
                                labList: labs,
                                contractorList: contractor,
                                states: states,
                                params: req.params
                            });
                        })
                    })
                })
            })
        });
    })
};

patientMaster.sendPDf = (req, res)=> {
    console.log("req.params.id ", req.params.id);
    patientService.findById(req.params.id).then(async(resp)=> {
        var patient = resp;
        patient['ethnicity'] = JSON.parse(patient.ethnicity);
        patient['insuranceInfo'] = JSON.parse(patient.insuranceInfo);
        patient['familydetails'] = JSON.parse(patient.familydetails);
        var filePath = "views/pages/template/patientPdf.ejs";
        var compiled =  await ejs.compile(fs.readFileSync(filePath, "utf8"));
        var mailObj = {};
        mailObj["to"] = config.mailer.pathTech;
        mailObj["subject"] = "Genetix New Applicant Requisition Form";
        mailObj["html"] = await compiled({data: patient, moment: moment});
        mailObj["filename"] = `${new Date().getTime()}-${patient.id}.pdf`;
        common.createPdf(mailObj["html"], mailObj["filename"]).then(resp=> {
            mailObj['filepath'] = resp.filename;
            // console.log("mailObj ", mailObj);
            common.sendMail(mailObj).then(resp=> {
                res.send({
                    status: true,
                    data: {},
                    msg: 'Mail send successfully'
                });
            }).catch(err=> {
                res.status(500).send({
                    status: false,
                    data: {},
                    msg: 'Something went wrong'
                })
            });
        })
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Records Found'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

patientMaster.downloadPCF = (req, res)=> {
    patientService.findById(req.params.id).then(async(resp)=> {
        var patient = resp;
        // physicianService.findById(patient.physicianId).then(async(resp)=>{
            patient['ethnicity'] = JSON.parse(patient.ethnicity);
            patient['insuranceInfo'] = JSON.parse(patient.insuranceInfo);
            patient['familydetails'] = JSON.parse(patient.familydetails);
            // patient['physician'] = resp;
            var filePath = "views/pages/template/patientConsentForm.ejs";
            var compiled =  await ejs.compile(fs.readFileSync(filePath, "utf8"));
            var pdfObj = {};
            pdfObj["html"] = await compiled({data: patient, moment: moment});
            pdfObj["filename"] = `${new Date().getTime()}-${patient.id}.pdf`;
            common.createPdf(pdfObj["html"], pdfObj["filename"]).then(resp=> {
                pdfObj['filepath'] = resp.filename;
                console.log("pdfObj['filepath'] ", pdfObj['filepath']);
                res.download(pdfObj['filepath'], function (err) {
                    if (err) {
                        console.log("Error");
                        console.log(err);
                    } else {
                        console.log("Success");
                    }    
                });
            })
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Records Found'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

patientMaster.sendSignatureLink = (req, res)=> {
    patientService.findById(req.params.id).then(async(resp)=> {
        var patient = resp;
        console.log("resp ===>sendResetLink", resp);
		var payload = {};
		payload['id'] = patient.id;
		payload['email'] = patient.email;
		
		var token = jwt.sign(payload, config.secret_key, { expiresIn: '12h' });
		var url = `${config.host}/get/patient/signature/${payload['id']}/${token}`;
		var filePath = 'views/pages/template/patientSignature.ejs';
		var compiled = ejs.compile(fs.readFileSync(filePath, 'utf8'));
		var mailObj = {};
		mailObj['to'] = payload['email'];
		mailObj['subject'] = 'Genetix DNA Screening Signature';
		mailObj['html'] = compiled({ link: url });
		common.sendMail(mailObj).then(resp=> {
			// req.toastr.success('Email Send To Registered Mail Id', "Success");
            // res.redirect('/login');
            var reqObj = {};
            reqObj['id'] = patient.id;
            reqObj['sign_token'] = token;
            patientService.findAndUpdate(reqObj).then(async(resp)=> {
                res.send({
                    status: true,
                    data: {},
                    msg: 'Mail send successfully'
                });
            }).catch(err=> {
                res.status(500).send({
                    status: false,
                    data: {},
                    msg: 'Something went wrong'
                })
            })
		}, err=> {
			res.status(417).send({
				status: false,
				data: {},
				msg: 'Failed to send a mail'
			})
		})
    }, err=> {
 console.log("err ", err);
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Records Found'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

patientMaster.sendSignatureLinkSms = (req, res)=> {
    console.log("eq.params.id =>sendSignatureLinkSms", req.params.id);
    patientService.findById(req.params.id).then(async(resp)=> {
        var patient = resp;
        console.log("resp ===>sendResetLink", resp);
		var payload = {};
		payload['id'] = patient.id;
        payload['email'] = patient.email;
        if (!patient['mobileNo']) {
            res.status(400).send({
				status: false,
				data: {},
				msg: "This patient can't have a Mobile Number"
			})
        }
		
		var token = jwt.sign(payload, config.secret_key, { expiresIn: '12h' });
		var url = `${config.host}/get/patient/signature/${payload['id']}/${token}`;
		var smsObj = {};
		smsObj['to'] = `+${patient['mobileNo'].replace('-','')}`;
		smsObj['body'] = `Please click on the below link and sign the DNA screening consent form using your finger. ${url}`;
		common.sendSMS(smsObj).then(resp=> {
			// req.toastr.success('Email Send To Registered Mail Id', "Success");
            // res.redirect('/login');
            var reqObj = {};
            reqObj['id'] = patient.id;
            reqObj['sign_token'] = token;
            patientService.findAndUpdate(reqObj).then(async(resp)=> {
                res.send({
                    status: true,
                    data: {},
                    msg: 'SMS send successfully'
                });
            }).catch(err=> {
                res.status(500).send({
                    status: false,
                    data: {},
                    msg: 'Something went wrong'
                })
            })
		}, err=> {
			res.status(417).send({
				status: false,
				data: {},
				msg: 'Failed to send a mail'
			})
		})
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: 'No Records Found'
        });
    }).catch(err=> {
        res.status(500).send({
            status: false,
            data: {},
            msg: 'Something went wrong'
        })
    })
};

patientMaster.redirectSingaturePage = (req, res)=> {
    jwt.verify(req.params.token, config.secret_key, function(err, decoded) {
		if (err) {
			res.status(401).send({
				status: false,
				data: {},
				msg: 'Invalid Token'
			});
		} else {
            patientService.findById(req.params.id).then(async(resp)=> {
                if (!resp.sign_token || resp.sign_token != req.params.token) {
                    res.status(401).send({
                        status: false,
                        data: {},
                        msg: 'Invalid token'
                    });
                    return false;
                }
                var url = req.protocol + '://' + req.get('host');
                res.render('pages/patient/signature', {data: req.params, url: url});
            }, err=> {
                    res.status(404).send({
                        status: false,
                        data: {},
                        msg: 'No Records Found'
                    });
            }).catch(err=> {
                res.status(500).send({
                    status: false,
                    data: {},
                    msg: 'Something went wrong'
                })
            })
		}
	});
};

patientMaster.cardEditForm = (req, res)=> {
    var reqObj = {};
    reqObj['id'] = req.session.user.id;
    // userService.findOne(reqObj).then(resp=> {
    //     var agentInfo = resp;
        patientService.findById(req.params.id).then(resp=> {
            var patient = resp;
            // patient['familydetails'] = JSON.parse(resp['familydetails']);
            // patient['diagnosedWithCancer'] = JSON.parse(resp['diagnosedWithCancer']);
            // patient['ashkenaziJewishAncestry'] = JSON.parse(resp['ashkenaziJewishAncestry']);
            // patient['colonPolyps'] = JSON.parse(resp['colonPolyps']);
            // patient['dnaCancerScreening'] = JSON.parse(resp['dnaCancerScreening']);
            // patient['ethnicity'] = JSON.parse(resp['ethnicity']);
            // patient['insuranceInfo'] = JSON.parse(resp['insuranceInfo']);
            patient['cardImages'] = JSON.parse(resp['cardImages']);
            // console.log("resp['physicianIds'] ", resp['physicianIds']);
            // patient['physicianIds'] = resp['physicianIds'] !=null ? JSON.parse(resp['physicianIds']): [];
            // console.log("patient['physicianIds']  ", patient['physicianIds'] );
            // physicianService.findMultipleIds(patient.physicianIds).then(resp=> {
            //     patient['physicians'] = resp;
            //     console.log("patient ==>", patient);
                // physicianService.findAllList({}).then(resp=> {
                //     var physicianList = resp;
                    // console.log("resp ==>physicianService", resp);
                    // labService.findAllList().then(labs=> {
                        // userService.findAllContractorList().then(contractor=> {
                            res.render('pages/patient/cardEdit', {
                                patient: patient,
                                // agent: agentInfo,
                                // physicianList: physicianList,
                                // labList: labs,
                                // contractorList: contractor,
                                // states: states,
                                params: req.params
                            });
                        // })
                    // })
                // })
            // })
        });
    // })
};

module.exports = patientMaster;