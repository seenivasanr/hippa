var userService = require('../services/userService');
var labService = require('../services/labService');
var physicianService = require('../services/physicianService');
var patientService = require('../services/patientService');
var swapkitService = require('../services/swapService.js');

var common = require("../common/common.js");
var states = require("../common/states");
var styles = require("../common/styles");
const config = require('../config/config')[process.env.NODE_ENV || "development"];

//import report data
var contractorReport = require('../reportSection/contractor');
var employeeReport = require('../reportSection/employee');
var labReport = require('../reportSection/lab');
var physicianReport = require('../reportSection/physician');
var patientReport = require('../reportSection/patient');
var swapkitReport = require('../reportSection/swapkit');
var newSubmissionReport = require('../reportSection/newSubmission');
var payrollReport = require('../reportSection/payroll');
var testResulted = require('../reportSection/testResulted');
var holdOnReport = require('../reportSection/holdon');
var insuranceReport = require('../reportSection/insurance');
var adHocReport = require('../reportSection/ad_hoc');

const ejs = require("ejs");
const fs = require("fs");
const randomstring = require("randomstring");
const excel = require('node-excel-export');
const _ = require('lodash');
const moment = require('moment')

var reportMaster = {};

// Reports View
reportMaster.viewReports = (req, res) => {
    res.render('pages/reports/report');
}


// Contractor Report
reportMaster.contractorList = (req, res) => {
	var reqObj = {};
	var pagination = {};
	pagination['perPage'] = 10;
	pagination['page'] = req.params.page || 1
	reqObj['role'] = 'contractor';
	reqObj['fullName'] = `%${req.query['fullName']? req.query['fullName']: ''}%`;
	reqObj['email'] = `%${req.query['email']? req.query['email']: ''}%`;
	reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
	reqObj['state'] = `%${req.query['state']? req.query['state']: ''}%`;
	reqObj['groupPracticeName'] = `%${req.query['groupPracticeName']? req.query['groupPracticeName']: ''}%`;


  	if(req.query['export'] == 'true'){
		// Get data set without Pagination
		reqObj['export'] = true;
		userService.findByRole(reqObj, pagination).then(resp=> {
			var report = excel.buildExport(							// Create Reports
				[ 
					{
						name: contractorReport.sheet_name, // Sheet Name
						heading: contractorReport.heading, 
						merges: contractorReport.merges, 
						specification: contractorReport.specification, 
						data: resp.rows
					}
				]
			);
			res.attachment(contractorReport.file_name); // File Name
			res.send(report);
		}, err=> { 
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		});		
	} else {
		userService.findByRole(reqObj, pagination).then(resp=> {
			res.render('pages/reports/contractor_report', {
				data: resp.rows,
				current: pagination['page'],
				pages: Math.ceil(resp.count / pagination['perPage']),
				perPage: pagination['perPage'],
				email : req.query['email'],
				gender : req.query['gender'],
				fullName : req.query['fullName'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			});
		}, err=> {
			res.render('pages/reports/contractor_report', {
				data: [],
				current: 1,
				pages: 1,
				email : req.query['email'],
				gender : req.query['gender'],
				fullName : req.query['fullName'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			})
		})
	}
};

reportMaster.employeeReport = (req, res)=> {
	var reqObj = {};
	var pagination = {};
	pagination['perPage'] = 10;
	pagination['page'] = req.params.page || 1
	reqObj['role'] = 'employee';
	reqObj['fullName'] = `%${req.query['fullName']? req.query['fullName']: ''}%`;
    reqObj['email'] = `%${req.query['email']? req.query['email']: ''}%`;
	reqObj['state'] = `%${req.query['state']? req.query['state']: ''}%`;
	reqObj['groupPracticeName'] = `%${req.query['groupPracticeName']? req.query['groupPracticeName']: ''}%`;

    var status = '';
    if (req.query['status']) {
        status = req.query['status'] == 'true' ? true : false;
        reqObj['status'] = status;
    } else {
        reqObj['status'] = '%%';
    }
	reqObj['permission'] = `%${req.query['permission']? req.query['permission']: ''}%`;
	reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
	console.log("req.query['export'] ", req.query['export']);
	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		userService.findByRole(reqObj, pagination).then(resp=> {
			var report = excel.buildExport(							// Create Reports
				[ 
					{
						name: employeeReport.sheet_name, // Sheet Name
						heading: employeeReport.heading, 
						merges: employeeReport.merges, 
						specification: employeeReport.specification, 
						data: resp.rows 
					}
				]
			);
			
			res.attachment(employeeReport.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		})
	} else {
		console.log('enter else ');
		userService.findByRole(reqObj, pagination).then(resp=> {
			//  console.log("resp ", resp);
			res.render('pages/reports/employee', {
				data: resp.rows,
				current: pagination['page'],
				pages: Math.ceil(resp.count / pagination['perPage']),
				perPage: pagination['perPage'],
				fullName : req.query['fullName'],
				email : req.query['email'],
				status : req.query['status'],
				permission : req.query['permission'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			});
		}, err=> {
			res.render('pages/reports/employee', {
				data: [],
				current: 1,
				pages: 1,
				fullName : req.query['fullName'],
				email : req.query['email'],
				status : req.query['status'],
				permission : req.query['permission'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			})
		})
	}
	
};

reportMaster.labReport = (req, res)=> {
	var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['labName'] = `%${req.query['labName']? req.query['labName']: ''}%`;
    reqObj['email'] = `%${req.query['email']? req.query['email']: ''}%`;
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
	console.log("reqObj ", reqObj);
	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		labService.findAll(reqObj, pagination).then(resp=> {
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: labReport.sheet_name, // Sheet Name
						heading: labReport.heading, 
						merges: labReport.merges, 
						specification: labReport.specification, 
						data: resp.rows 
					}
				]
			);
			
			res.attachment(labReport.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		})
	} else {
		labService.findAll(reqObj, pagination).then(resp=> {
			res.render('pages/reports/lab', {
				data: resp.rows,
				current: pagination['page'],
				pages: Math.ceil(resp.count / pagination['perPage']),
				perPage: pagination['perPage'],
				labName : req.query['labName'],
				email : req.query['email'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			});
		}, err=> {
			res.render('pages/reports/lab', {
				data: [],
				current: 1,
				pages: 1,
				labName : req.query['labName'],
				email : req.query['email'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			});
		}).catch(err=> {
			// re
		})
	}
};

reportMaster.physicianReport = (req, res) => {
	var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['name'] = `%${req.query['name']? req.query['name']: ''}%`;
	reqObj['email'] = `%${req.query['email']? req.query['email']: ''}%`;
	reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];

	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		physicianService.findAll(reqObj, pagination).then(resp=> {
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: physicianReport.sheet_name, // Sheet Name
						heading: physicianReport.heading, 
						merges: physicianReport.merges, 
						specification: physicianReport.specification, 
						data: resp.rows 
					}
				]
			);
			
			res.attachment(physicianReport.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		})
	} else {
		physicianService.findAll(reqObj, pagination).then(resp=> {
			res.render('pages/reports/physician', {
				data: resp.rows,
				current: pagination['page'],
				pages: Math.ceil(resp.count / pagination['perPage']),
				perPage: pagination['perPage'],
				name : req.query['name'],
				email : req.query['email'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			})
		}, err=> {
			res.render('pages/reports/physician', {
				data: [],
				current: 1,
				pages: 0,
				name : req.query['name'],
				email : req.query['email'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			})
		})
	}
};


reportMaster.patientReport = (req, res)=> {
    var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    
    var data = {};
    data['agentId'] = req.session.user.id;
    data['role'] = req.session.user.role;
    // data['notPaidStatus'] = true;
    
    if (data['role'] == 'admin') {
        data['name'] = "%%";
        data['contractorName'] = `%${req.query['name']? req.query['name'].trim(): ''}%`;
    } else {
        data['name'] = `%${req.query['name']? req.query['name'].trim(): ''}%`;
        data['contractorName'] = "%%";
    }
	data['testStatus'] = `%${req.query['testStatus']? req.query['testStatus']: ''}%`;
	// data['notPaidStatus'] = req.query['testStatus']? false: true;
    // data['formStatus'] = `%%`;
    data['submissionDate'] = req.query['submissionDate'];
    data['dob'] = req.query['dob'];
    data['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
	console.log("data ", data);
	
	if(req.query['export'] == 'true'){
		data['export'] = true;
		patientService.findAll(data, pagination).then(resp=> {
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: patientReport.sheet_name, // Sheet Name
						heading: patientReport.heading, 
						merges: patientReport.merges, 
						specification: patientReport.specification, 
						data: resp.rows 
					}
				]
			);
			
			res.attachment(patientReport.file_name); // File Name
			res.send(report);
		}, err=> {
			console.log('test', err);
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		});
	} else {
		patientService.findAll(data, pagination).then(resp=> {
			res.render('pages/reports/patient', {
				data: resp.rows,
				current: pagination['page'],
				pages: Math.ceil(resp.count / pagination['perPage']),
				perPage: pagination['perPage'],
				name: req.query['name']? req.query['name']:'',
				testStatus: req.query['testStatus']? req.query['testStatus']: '',
				submissionDate: req.query['submissionDate']? req.query['submissionDate']: '',
				dob: req.query['dob']? req.query['dob']: '',
				gender: req.query['gender']? req.query['gender']: '',
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			});
		}, err=> {
			res.render('pages/reports/patient', {
				data: [],
				current: 1,
				pages: 1,
				name: req.query['name']? req.query['name']:'',
				testStatus: req.query['testStatus']? req.query['testStatus']: '',
				submissionDate: req.query['submissionDate']? req.query['submissionDate']: '',
				dob: req.query['dob']? req.query['dob']: '',
				gender: req.query['gender']? req.query['gender']: '',
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			});
		}).catch(err=> {

		})
	}
};


reportMaster.swapkitReport = (req, res)=> {
    var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['patientName'] = `%${req.query['patientName']? req.query['patientName']: ''}%`;
    reqObj['orderStatus'] = req.query['orderStatus']? req.query['orderStatus']: '%%';
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
	console.log("reqObj ", reqObj);
	
	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		swapkitService.findAll(reqObj, pagination).then(resp=> {
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: swapkitReport.sheet_name, // Sheet Name
						heading: swapkitReport.heading, 
						merges: swapkitReport.merges, 
						specification: swapkitReport.specification, 
						data: resp.rows 
					}
				]
			);
			
			res.attachment(swapkitReport.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		});
	} else {
		swapkitService.findAll(reqObj, pagination).then(resp=> {
			res.render('pages/reports/swapkit', {
				data: resp.rows,
				current: pagination['page'],
				pages: Math.ceil(resp.count / pagination['perPage']),
				perPage: pagination['perPage'],
				patientName : req.query['patientName'],
				orderStatus : req.query['orderStatus'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			});
		}, err=> {
			res.render('pages/reports/swapkit', {
				data: [],
				current: 1,
				pages: 1,
				patientName : req.query['patientName'],
				orderStatus : req.query['orderStatus'],
				orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
				queryUrl: req._parsedOriginalUrl.search
			});
		}).catch(err=> {
			// re
		})
		
	}
};

reportMaster.newSubmissionReport = (req, res)=> {
	var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['testStatus'] = `${req.query['testStatus']? req.query['testStatus']: '%%'}`;
    reqObj['labId'] = req.query['labId']? req.query['labId']: '';
    reqObj['contractorId'] = req.query['contractorId']? req.query['contractorId']: '';
    reqObj['physicianId'] = req.query['physicianId']? req.query['physicianId']: '';
    // reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    reqObj['orderBy'] = req.query['orderBy']? JSON.parse(req.query['orderBy']): [];
    reqObj['daterange'] = req.query['daterange']? req.query['daterange']: '';
	console.log("reqObj ==>newSubmissionReport", reqObj);
	
	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		patientService.findAndGroupPatient(reqObj, pagination).then(resp=> {
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: newSubmissionReport.sheet_name, // Sheet Name
						heading: newSubmissionReport.heading, 
						merges: newSubmissionReport.merges, 
						specification: newSubmissionReport.specification, 
						data: resp.rows 
					}
				]
			);
			
			res.attachment(newSubmissionReport.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		});
	} else {
		physicianService.findAllList({}).then(physicians=> {
			labService.findAllList().then(labs=> {
				userService.findAllContractorList().then(contractors=> {
					patientService.findAndGroupPatient(reqObj,pagination).then(resp=> {
						var data = _.orderBy(resp.rows, [reqObj['orderBy'][0] ], [reqObj['orderBy'][1]]);
 						console.log("data ", data);

						res.render('pages/reports/new_submission', {
							data: data,
							current: pagination['page'],
							pages: Math.ceil(1 / pagination['perPage']),
							perPage: pagination['perPage'],
							testStatus : req.query['testStatus'],
							labId : req.query['labId'],
							contractorId : req.query['contractorId'],
							physicianId : req.query['physicianId'],
							daterange: req.query['daterange'],
							orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
							physicianList: physicians,
							labList: labs,
							contractorList: contractors,
							queryUrl: req._parsedOriginalUrl.search
						});
					});
				});
			});
		});
	}
};

reportMaster.payrollReport = (req, res)=> {
	var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['testStatus'] = `${req.query['testStatus']? req.query['testStatus']: '%%'}`;
    reqObj['labId'] = req.query['labId']? req.query['labId']: '';
    reqObj['contractorId'] = req.query['contractorId']? req.query['contractorId']: '';
    reqObj['physicianId'] = req.query['physicianId']? req.query['physicianId']: '';
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    reqObj['daterange'] = req.query['daterange']? req.query['daterange']: '';
	console.log("reqObj ", reqObj);
	
	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		patientService.findPayrollReport(reqObj, pagination).then(resp=> {
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: payrollReport.sheet_name, // Sheet Name
						heading: payrollReport.heading, 
						merges: payrollReport.merges, 
						specification: payrollReport.specification, 
						data: resp.rows 
					}
				]
			);
			
			res.attachment(payrollReport.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		});
	} else {
		physicianService.findAllList({}).then(physicians=> {
			labService.findAllList().then(labs=> {
				userService.findAllContractorList().then(contractors=> {
					patientService.findPayrollReport(reqObj, pagination).then(resp=> {
						res.render('pages/reports/payroll', {
							data: resp.rows,
							current: pagination['page'],
							pages: Math.ceil(resp.count / pagination['perPage']),
							perPage: pagination['perPage'],
							testStatus : req.query['testStatus'],
							labId : req.query['labId'],
							contractorId : req.query['contractorId'],
							physicianId : req.query['physicianId'],
							daterange: req.query['daterange'],
							orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
							physicianList: physicians,
							labList: labs,
							contractorList: contractors,
							queryUrl: req._parsedOriginalUrl.search
						});
					});
				});
			});
		});
	}
};

reportMaster.ResultedReport = (req, res)=> {
	var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['testStatus'] = `${req.query['testStatus']? req.query['testStatus']: '%%'}`;
    reqObj['labId'] = req.query['labId']? req.query['labId']: '';
    reqObj['contractorId'] = req.query['contractorId']? req.query['contractorId']: '';
    reqObj['physicianId'] = req.query['physicianId']? req.query['physicianId']: '';
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    reqObj['daterange'] = req.query['daterange']? req.query['daterange']: '';
	console.log("reqObj ==>ResultedReport", reqObj);
	
	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		patientService.findPayrollReport(reqObj, pagination).then(resp=> {
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: testResulted.sheet_name, // Sheet Name
						heading: testResulted.heading, 
						merges: testResulted.merges, 
						specification: testResulted.specification, 
						data: resp.rows 
					}
				]
			);
			
			res.attachment(testResulted.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		});
	} else {
		physicianService.findAllList({}).then(physicians=> {
			labService.findAllList().then(labs=> {
				userService.findAllContractorList().then(contractors=> {
					patientService.findPayrollReport(reqObj, pagination).then(resp=> {
 						console.log("resp ", resp);
						res.render('pages/reports/test_resulted', {
							data: resp.rows,
							current: pagination['page'],
							pages: Math.ceil(resp.count / pagination['perPage']),
							perPage: pagination['perPage'],
							testStatus : req.query['testStatus'],
							labId : req.query['labId'],
							contractorId : req.query['contractorId'],
							physicianId : req.query['physicianId'],
							daterange: req.query['daterange'],
							orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
							physicianList: physicians,
							labList: labs,
							contractorList: contractors,
							queryUrl: req._parsedOriginalUrl.search
						});
					});
				});
			});
		});
	}
};

reportMaster.holdOnReport = (req, res)=> {
	var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['testStatus'] = `${req.query['testStatus']? req.query['testStatus']: '%%'}`;
    reqObj['labId'] = req.query['labId']? req.query['labId']: '';
    reqObj['contractorId'] = req.query['contractorId']? req.query['contractorId']: '';
    reqObj['physicianId'] = req.query['physicianId']? req.query['physicianId']: '';
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    reqObj['daterange'] = req.query['daterange']? req.query['daterange']: '';
	console.log("reqObj ==>ResultedReport", reqObj);
	
	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		patientService.findPayrollReport(reqObj, pagination).then(resp=> {
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: holdOnReport.sheet_name, // Sheet Name
						heading: holdOnReport.heading, 
						merges: holdOnReport.merges, 
						specification: holdOnReport.specification, 
						data: resp.rows 
					}
				]
			);
			
			res.attachment(holdOnReport.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		});
	} else {
		physicianService.findAllList({}).then(physicians=> {
			labService.findAllList().then(labs=> {
				userService.findAllContractorList().then(contractors=> {
					patientService.findPayrollReport(reqObj, pagination).then(resp=> {
 						console.log("resp ", resp);
						res.render('pages/reports/holdon', {
							data: resp.rows,
							current: pagination['page'],
							pages: Math.ceil(resp.count / pagination['perPage']),
							perPage: pagination['perPage'],
							testStatus : req.query['testStatus'],
							labId : req.query['labId'],
							contractorId : req.query['contractorId'],
							physicianId : req.query['physicianId'],
							daterange: req.query['daterange'],
							orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
							physicianList: physicians,
							labList: labs,
							contractorList: contractors,
							queryUrl: req._parsedOriginalUrl.search
						});
					});
				});
			});
		});
	}
};

reportMaster.insuranceReport = (req, res)=> {
	var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['testStatus'] = `${req.query['testStatus']? req.query['testStatus']: '%%'}`;
    reqObj['preInsuranceType'] = `${req.query['insuranceType']? req.query['insuranceType']: '%%'}`;
    reqObj['labId'] = req.query['labId']? req.query['labId']: '';
    reqObj['contractorId'] = req.query['contractorId']? req.query['contractorId']: '';
    reqObj['physicianId'] = req.query['physicianId']? req.query['physicianId']: '';
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    reqObj['daterange'] = req.query['daterange']? req.query['daterange']: '';
    reqObj['state'] = 'insurance';
	console.log("reqObj ==>ResultedReport", reqObj);
	
	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		patientService.findPayrollReport(reqObj, pagination).then(resp=> {
			var data = resp.rows.filter(item => {
				if(item.testStatus == 'Dr.Approval Pending'){
					item.statusBasedDayDiff = moment(moment(item.approvalPendingDate).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Doctor Approved'){
					item.statusBasedDayDiff = moment(moment(item.doctorApproved).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Accepted'){
					item.statusBasedDayDiff = moment(moment(item.accepted).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test on Hold'){
					item.statusBasedDayDiff = moment(moment(item.onHold).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Shipped to Lab'){
					item.statusBasedDayDiff = moment(moment(item.shippedtoLab).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Received by Lab'){
					item.statusBasedDayDiff = moment(moment(item.receivedbyLab).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Inprogress at Lab'){
					item.statusBasedDayDiff = moment(moment(item.inProgressatLab).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Resulted'){
					item.statusBasedDayDiff = moment(moment(item.resulted).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				}

				if(item.resulted){
					item.resultedDayDiff = moment(moment(item.resulted).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				}

				return item;
			});
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: insuranceReport.sheet_name, // Sheet Name
						heading: insuranceReport.heading,
						merges: insuranceReport.merges,
						specification: insuranceReport.specification,
						data: data
					}
				]
			);
			
			res.attachment(insuranceReport.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		});
	} else {
		physicianService.findAllList({}).then(physicians=> {
			labService.findAllList().then(labs=> {
				userService.findAllContractorList().then(contractors=> {
					patientService.findPayrollReport(reqObj, pagination).then(resp=> {
 						console.log("resp ==>insuranceReport", resp);
						res.render('pages/reports/insurance', {
							data: resp.rows,
							current: pagination['page'],
							pages: Math.ceil(resp.count / pagination['perPage']),
							perPage: pagination['perPage'],
							testStatus : req.query['testStatus'],
							insuranceType: req.query['insuranceType'],
							labId : req.query['labId'],
							contractorId : req.query['contractorId'],
							physicianId : req.query['physicianId'],
							daterange: req.query['daterange'],
							orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
							physicianList: physicians,
							labList: labs,
							contractorList: contractors,
							queryUrl: req._parsedOriginalUrl.search
						});
					});
				});
			});
		});
	}
};

reportMaster.adHocReport = (req, res)=> {
	var pagination = {};
    pagination['perPage'] = 10;
    pagination['page'] = req.params.page || 1;
    var reqObj = {};
    reqObj['testStatus'] = `${req.query['testStatus']? req.query['testStatus']: '%%'}`;
    // reqObj['preInsuranceType'] = `${req.query['insuranceType']? req.query['insuranceType']: '%%'}`;
    reqObj['labId'] = req.query['labId']? req.query['labId']: '';
    reqObj['contractorId'] = req.query['contractorId']? req.query['contractorId']: '';
    reqObj['physicianId'] = req.query['physicianId']? req.query['physicianId']: '';
    reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    reqObj['daterange'] = req.query['daterange']? req.query['daterange']: '';
    // reqObj['state'] = 'insurance';
	console.log("reqObj ==>ResultedReport", reqObj);
	
	if(req.query['export'] == 'true'){
		reqObj['export'] = true;
		patientService.findPayrollReport(reqObj, pagination).then(resp=> {
			var data = resp.rows.filter(item => {
				if(item.testStatus == 'Dr.Approval Pending'){
					item.statusBasedDayDiff = moment(moment(item.approvalPendingDate).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Doctor Approved'){
					item.statusBasedDayDiff = moment(moment(item.doctorApproved).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Accepted'){
					item.statusBasedDayDiff = moment(moment(item.accepted).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test on Hold'){
					item.statusBasedDayDiff = moment(moment(item.onHold).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Shipped to Lab'){
					item.statusBasedDayDiff = moment(moment(item.shippedtoLab).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Received by Lab'){
					item.statusBasedDayDiff = moment(moment(item.receivedbyLab).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Inprogress at Lab'){
					item.statusBasedDayDiff = moment(moment(item.inProgressatLab).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Test Resulted'){
					item.statusBasedDayDiff = moment(moment(item.resulted).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				} else if(item.testStatus == 'Paid') {
					item.statusBasedDayDiff = moment(moment(item.Paid).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				}

				if(item.resulted){
					item.resultedDayDiff = moment(moment(item.resulted).format('MMM-DD-YYYY')).diff(moment(item.submissionDate).format('MMM-DD-YYYY'),'days')
				}

				return item;
			});
			var report = excel.buildExport(	// Create Reports
				[ 
					{
						name: adHocReport.sheet_name, // Sheet Name
						heading: adHocReport.heading,
						merges: adHocReport.merges,
						specification: adHocReport.specification,
						data: data
					}
				]
			);
			
			res.attachment(adHocReport.file_name); // File Name
			res.send(report);
		}, err=> {
			res.status(404).send({
				status: false,
				data: {},
				msg: 'No Records Found'
			})
		}).catch(err=> {
			res.status(500).send({
				status: false,
				data: {},
				msg: 'Something went wrong'
			})
		});
	} else {
		physicianService.findAllList({}).then(physicians=> {
			labService.findAllList().then(labs=> {
				userService.findAllContractorList().then(contractors=> {
					patientService.findPayrollReport(reqObj, pagination).then(resp=> {
 						console.log("resp ==>adHocReport", resp);
						res.render('pages/reports/ad_hoc', {
							data: resp.rows,
							current: pagination['page'],
							pages: Math.ceil(resp.count / pagination['perPage']),
							perPage: pagination['perPage'],
							testStatus : req.query['testStatus'],
							// insuranceType: req.query['insuranceType'],
							labId : req.query['labId'],
							contractorId : req.query['contractorId'],
							physicianId : req.query['physicianId'],
							daterange: req.query['daterange'],
							orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
							physicianList: physicians,
							labList: labs,
							contractorList: contractors,
							queryUrl: req._parsedOriginalUrl.search
						});
					});
				});
			});
		});
	}
};

module.exports = reportMaster;