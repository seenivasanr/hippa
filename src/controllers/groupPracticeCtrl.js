var userService = require('../services/userService');

var common = require("../common/common.js");
var states = require("../common/states");
const config = require('../config/config')[process.env.NODE_ENV || "development"];

const ejs = require("ejs");
const fs = require("fs");
const randomstring = require("randomstring");

var groupMaster = {};

groupMaster.create = (req, res)=> {
    var data = req.body;
    !data['dob'] && delete data['dob'];
    console.log("data ==>create group", data);
    // return false;
    var random = randomstring.generate(6);
    console.log("random ", random);
    common.genHash(random).then(secret => {
        data["password"] = secret;
        userService.findAndCreate(data).then(resp => {
        var filePath = "views/pages/template/password.ejs";
        var compiled = ejs.compile(fs.readFileSync(filePath, "utf8"));
        var mailObj = {};
        mailObj["to"] = data["email"];
        mailObj["subject"] = "Genetix HIPAA - Welcome";
        mailObj["html"] = compiled({data: data, random: random, baseUrl: config.host});
        common.sendMail(mailObj);
        req.toastr.success("Created Successfully", "Success");
        res.redirect("/groupPractice/list/1");
        }, err => {
        req.toastr.error("User Name Already Exists", "Error");
        req.app.locals.obj = req.body;
        // res.redirect("/contractor/list/1");
        res.redirect(`/groupPractice/add/${req.body.position}/1`);
        });
    });
};

groupMaster.list = (req, res)=> {
    var reqObj = {};
	var pagination = {};
	pagination['perPage'] = 10;
	pagination['page'] = req.params.page || 1
	reqObj['role'] = 'physician';
	// reqObj['gender'] = `%${req.query['gender']? req.query['gender']: ''}%`;
	reqObj['fullName'] = `%${req.query['fullName']? req.query['fullName']: ''}%`;
	reqObj['state'] = `%${req.query['state']? req.query['state']: ''}%`;
	reqObj['groupPracticeName'] = `%${req.query['groupPracticeName']? req.query['groupPracticeName']: ''}%`;
	reqObj['orderBy'] = req.query['orderBy']? [JSON.parse(req.query['orderBy'])]: [];
    console.log("req.query ===>", req.query);
	userService.findByRole(reqObj, pagination).then(resp=> {
		//  console.log("resp ", resp);
		res.render('pages/groupPractice/list', {
			data: resp.rows,
			current: pagination['page'],
			pages: Math.ceil(resp.count / pagination['perPage']),
            perPage: pagination['perPage'],
            state : req.query['state'],
            groupPracticeName : req.query['groupPracticeName'],
            gender : req.query['gender'],
            fullName : req.query['fullName'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search,
            states: states
        });
	}, err=> {
		res.render('pages/groupPractice/list', {
			data: [],
			current: 1,
            pages: 1,
            email : req.query['email'],
            groupPracticeName : req.query['groupPracticeName'],
            gender : req.query['gender'],
            fullName : req.query['fullName'],
            orderBy: req.query['orderBy'] ? JSON.parse(req.query['orderBy']): [],
            queryUrl: req._parsedOriginalUrl.search
                })
        })
};

groupMaster.delete = (req, res)=> {
    var reqObj = {};
    reqObj['id'] = req.query.id;
    userService.findAndDelete(reqObj).then(resp=> {
        res.send({
            status: true,
            msg: 'Deleted Successfully'
        });
    }, err=> {
        res.status(404).send({
            status: false,
            data: {},
            msg: err
        });
    }).catch(err=> {
        res.status(500).send({
        status: false,
        data: {},
        msg: 'Something went wrong'
        })
    })
};

groupMaster.editView = (req, res)=> {
    console.log("req.params ", req.params);
    userService.findOne({id: req.params.id}).then(resp=> {
      res.render('pages/groupPractice/edit',{
        data: resp,
        params: req.params,
        states: states
      })
    }, err=> {
      res.redirect(404,`/groupPractice/list/${req.params.page}`);
    }).catch(err=> {
      res.redirect(500,`/groupPractice/list/${req.params.page}`);
    })
};

groupMaster.update = (req, res)=> {
    !req.body['dob'] && delete req.body['dob'];
    userService.findOneAndUpdateProfile(req.body).then(resp=> {
      res.send({
        status: true,
        data:resp,
        msg: "Updated Successfully"
      })
    }, err=> {
      res.status(400).send({
        status: false,
        data: {},
        msg: 'Invalid Request'
      });
    }).catch(err=> {
      res.status(500).send({
        status: false,
        data: {},
        msg: 'Something went wrong'
      })
    })
};

groupMaster.viewGroup = (req, res)=> {
  userService.findOne({id: req.params.id}).then(resp=> {
    console.log("resp viewGroup", resp);
    res.render('pages/groupPractice/view',{
      data: resp
    })
  }, err=> {
    res.redirect(404,'/groupPractice/list/1');
  }).catch(err=> {
    res.redirect(500,'/groupPractice/list/1');
  })
};


module.exports = groupMaster;