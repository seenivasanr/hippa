'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Patients', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      fullName: {
        type: Sequelize.STRING,
        get() {
          return `${this.firstName} ${this.lastName}`;
        },
        set(value) {
          throw new Error('Do not try to set the `fullName` value!');
        }
      },
      dob: {
        type: Sequelize.DATE
      },
      phoneNo: {
        type: Sequelize.STRING
      },
      
      address: {
        type: Sequelize.STRING
      },
      mi: {
        type: Sequelize.STRING
      },
      timeToCall: {
        type: Sequelize.STRING
      },
      state: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      zipcode: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING,
        // unique: true,
        // allowNull: false,
        // validate: {
        //   notNull: {
        //     msg: 'Please enter email'
        //   }
        // }
      },
      diagnosedWithCancer: {
        type: Sequelize.STRING
      },
      ashkenaziJewishAncestry: {
        type: Sequelize.STRING
      },
      colonPolyps: {
        type: Sequelize.STRING
      },
      dnaCancerScreening: {
        type: Sequelize.STRING
      },
      familydetails: {
        type: Sequelize.TEXT('long')
      },
      formbuilderFields: {
        type: Sequelize.TEXT('long')
      },
      ethnicity: {
        type: Sequelize.TEXT('long')
      },
      insuranceInfo: {
        type: Sequelize.TEXT('long')
      },
      cardImages: {
        type: Sequelize.TEXT('long'),
      },
      signature: {
        type: Sequelize.TEXT('long')
      },
      pSignature: {
        type: Sequelize.TEXT('long')
      },
      paymentCopy: {
        type: Sequelize.TEXT('long')
      },
      paymentStatus: {
        type: Sequelize.STRING
      },
      preInsuranceType: {
        type: Sequelize.STRING
      },
      secInsuranceType: {
        type: Sequelize.STRING
      },
      prepolicy: {
        type: Sequelize.STRING
      },
      secpolicy: {
        type: Sequelize.STRING
      },
      physicianIds: {
        type: Sequelize.STRING
      },
      testName: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.ENUM,
        values: ['male', 'female', 'others']
      },
      createdBy: {
        type: Sequelize.ENUM,
        values: ['employee', 'contractor','physician']
      },
      createrId: {
        type: Sequelize.INTEGER
      },
      agentId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Users',
          key: 'id'
        }
      },
      labId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Labs',
          key: 'id'
        }
      },
      physicianId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Physicians',
          key: 'id'
        }
      },
      testStatus: {
        type: Sequelize.ENUM,
        values: ['Dr.Approval Pending', 'Doctor Approved', 'Test Accepted', 'Test on Hold', 'Test Shipped to Lab', 'Test Received by Lab', 'Test Inprogress at Lab', 'Test Resulted', 'Paid']
      },
      formStatus: {
        type: Sequelize.STRING
      },
      swapkitshipedTo: {
        type: Sequelize.ENUM,
        values: ['home', 'physician','pharmacy']
      },
      DeletedAt: {
        type: Sequelize.DATE
      },
      approvalPendingDate: {
        type: Sequelize.DATE
      },
      doctorApproved: {
        type: Sequelize.DATE
      },
      accepted: {
        type: Sequelize.DATE
      },
      onHold: {
        type: Sequelize.DATE
      },
      shippedtoLab: {
        type: Sequelize.DATE
      },
      receivedbyLab: {
        type: Sequelize.DATE
      },
      inProgressatLab: {
        type: Sequelize.DATE
      },
      resulted: {
        type: Sequelize.DATE
      },
      Paid: {
        type: Sequelize.DATE
      },
      submissionDate: {
        type: Sequelize.DATE
      },
      signatureFile: {
        type: Sequelize.STRING
      },
      signatureFileName: {
        type: Sequelize.STRING
      },
      sign_date: {
        type: Sequelize.DATE
      },
      sign_token: {
        type: Sequelize.TEXT('long')
      },
      mobileNo: {
        type: Sequelize.STRING
      },
      reasonForHoldOn: {
        type: Sequelize.STRING
      },
      phy_date: {
        type: Sequelize.DATE
      },
      phy_name: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Patients');
  }
};