'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fullName: {
        type: Sequelize.STRING
      },
      pharmacyName: {
        type: Sequelize.STRING
      },
      constractorId: {
        type: Sequelize.STRING,
        unique: true
      },
      employeeNo: {
        type: Sequelize.STRING,
        unique: true
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notNull: {
            msg: 'Please enter email'
          }
        }
      },
      pharmacyNpi: {
        type: Sequelize.STRING
      },
      businessEin: {
        type: Sequelize.STRING
      },
      streetAddress: {
        type: Sequelize.STRING
      },
      phoneNo: {
        type: Sequelize.STRING
      },
      userName: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
        validate: {
          notNull: {
            msg: 'Please Enter User Name'
          }
        }
      },
      FaxNo: Sequelize.STRING,
      state: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      zipcode: {
        type: Sequelize.STRING
      },
      ncpa: {
        type: Sequelize.STRING
      },
      partnerName: {
        type: Sequelize.STRING
      },
      partnerId: {
        type: Sequelize.STRING
      },
      image_url: {
        type: Sequelize.STRING
      },
      dob: {
        type: Sequelize.DATE
      },
      password: {
        type: Sequelize.STRING
      },
      gender: {
        type: Sequelize.ENUM,
        values: ['male', 'female', 'others']
      },
      role: {
        type: Sequelize.ENUM,
        values:['admin','contractor','employee', 'physician']
      },
      position: {
        type: Sequelize.ENUM,
        values:['interviewer','viewer','agent']
      },
      permission: {
        type: Sequelize.ENUM,
        values:['canview','canedit']
      },
      status: {
        type: Sequelize.BOOLEAN
      },
      isActive: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      IsDelete: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      DeletedAt: {
        type: Sequelize.DATE
      },
      healthSystem: {
        type: Sequelize.STRING
      },
      groupPracticeName: {
        type: Sequelize.STRING
      },
      licenseNo: {
        type: Sequelize.STRING
      },
      npiNo: {
        type: Sequelize.STRING
      },
      einNo: {
        type: Sequelize.STRING
      },
      primaryNo: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};