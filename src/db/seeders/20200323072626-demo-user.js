'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
    */
      return queryInterface.bulkInsert('Users', [{
        fullName: 'Admin',
        email: 'admin@info.com',
        streetAddress: "chennai",
        phoneNo: '9078568756',
        userName: 'Admin',
        state: 'NY',
        city: 'Chennai',
        zipcode: '34333-3333',
        role: 'admin',
        gender: 'male',
        password: '$2a$08$qhyl4Cjyma4ovVGiIDjJFOSAAy8rZGhS4yb03ck3f4xnvKhDe5EP2',
      }], {});
      // return [
        // return queryInterface.addColumn('Patients', 'preInsuranceType', {
        //   type: Sequelize.STRING
        // }),
        // queryInterface.addColumn('Patients', 'prepolicy', {
        //   type: Sequelize.STRING,
        // }),
        // queryInterface.addColumn('Patients', 'secInsuranceType', {
        //   type: Sequelize.STRING,
        // }),
        // queryInterface.addColumn('Patients', 'secpolicy', {
        //   type: Sequelize.STRING,
        // })

      // ]
   
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
