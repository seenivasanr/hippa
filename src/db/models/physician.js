'use strict';
module.exports = (sequelize, DataTypes) => {
  const Physician = sequelize.define('Physician', {
    healthSystem: DataTypes.STRING,
    groupPracticeName: DataTypes.STRING,
    name: {
      type: DataTypes.STRING,
      unique: true
    },
    email: {
      type: DataTypes.STRING,
      // unique: true,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please enter email'
        }
      }
    },
    licenseNo: DataTypes.STRING,
    npiNo: DataTypes.STRING,
    phoneNo: DataTypes.STRING,
    FaxNo: DataTypes.STRING,
    address: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    zipcode: DataTypes.STRING,
    DeletedAt: DataTypes.DATE
  }, {});
  Physician.associate = function(models) {
    // associations can be defined here
  };
  return Physician;
};