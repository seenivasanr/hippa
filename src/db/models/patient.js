'use strict';
module.exports = (sequelize, DataTypes) => {
  const Patient = sequelize.define('Patient', {
    fullName: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    dob: DataTypes.DATE,
    gender: {
      type: DataTypes.ENUM,
      values: ['male', 'female', 'others']
    },
    phoneNo: DataTypes.STRING,
    address: DataTypes.STRING,
    mi: DataTypes.STRING,
    timeToCall: DataTypes.STRING,
    state: DataTypes.STRING,
    city: DataTypes.STRING,
    zipcode: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      // unique: true,
      // allowNull: false,
      // validate: {
      //   notNull: {
      //     msg: 'Please enter email'
      //   }
      // }
    },
    diagnosedWithCancer: DataTypes.STRING,
    ashkenaziJewishAncestry: DataTypes.STRING,
    colonPolyps: DataTypes.STRING,
    dnaCancerScreening: DataTypes.STRING,
    familydetails: DataTypes.TEXT('long'),
    formbuilderFields: DataTypes.TEXT('long'),
    ethnicity: DataTypes.TEXT('long'),
    insuranceInfo: DataTypes.TEXT('long'),
    cardImages: DataTypes.TEXT('long'),
    signature: DataTypes.TEXT('long'),
    pSignature: DataTypes.TEXT('long'),
    paymentCopy: DataTypes.TEXT('long'),
    paymentStatus: DataTypes.STRING,
    preInsuranceType: DataTypes.STRING,
    secInsuranceType: DataTypes.STRING,
    prepolicy: DataTypes.STRING,
    secpolicy: DataTypes.STRING,
    physicianIds: DataTypes.STRING,
    testName: DataTypes.STRING,
    createdBy: {
      type: DataTypes.ENUM,
      values: ['employee', 'contractor', 'physician']
    },
    createrId: {
      type: DataTypes.INTEGER
    },
    agentId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      }
    },
    labId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Labs',
        key: 'id'
      }
    },
    physicianId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Physicians',
        key: 'id'
      }
    },
    testStatus: {
      type: DataTypes.ENUM,
      values: ['Dr.Approval Pending', 'Doctor Approved', 'Test Accepted', 'Test on Hold', 'Test Shipped to Lab', 'Test Received by Lab', 'Test Inprogress at Lab', 'Test Resulted', 'Paid']
    },
    formStatus: {
      type: DataTypes.STRING
    },
    swapkitshipedTo: {
      type: DataTypes.ENUM,
      values: ['home', 'physician','pharmacy']
    },
    DeletedAt: DataTypes.DATE,
    approvalPendingDate: DataTypes.DATE,
    doctorApproved: DataTypes.DATE,
    accepted: DataTypes.DATE,
    onHold: DataTypes.DATE,
    shippedtoLab: DataTypes.DATE,
    receivedbyLab: DataTypes.DATE,
    inProgressatLab: DataTypes.DATE,
    resulted: DataTypes.DATE,
    Paid: DataTypes.DATE,
    submissionDate: DataTypes.DATE,
    signatureFile: DataTypes.STRING,
    signatureFileName: DataTypes.STRING,
    sign_date: DataTypes.DATE,
    sign_token: DataTypes.TEXT('long'),
    mobileNo: DataTypes.STRING,
    reasonForHoldOn: DataTypes.STRING,
    phy_date: DataTypes.DATE,
    phy_name: DataTypes.STRING,
  }, {});
  Patient.associate = function(models) {
    // associations can be defined here
    // Patient.belongsTo(models.Users, {
    //   foreignKey: 'userId',
    //   targetKey: 'id'
    // });

    // Patient.belongsTo(models.Labs, {
    //   foreignKey: 'labId',
    //   targetKey: 'id'
    // })
    Patient.belongsTo(models.Labs, {
      as: 'lab',
      foreignKey: 'labId',
      targetKey: 'id'
    });
    Patient.belongsTo(models.Physician, {
      as: 'physician',
      foreignKey: 'physicianId',
      targetKey: 'id'
    });

    Patient.belongsTo(models.Users, {
      as: 'agent',
      foreignKey: 'agentId',
      targetKey: 'id'
    });
    // Patient.hasMany(models.Labs, {as: 'Singer'});


    // Patient.hasMany(models.Swapkits, {
    //   foreignKey: 'patiantId',
    //   sourceKey: 'id'
    // });
  };
  return Patient;
};