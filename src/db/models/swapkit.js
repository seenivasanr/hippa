'use strict';
module.exports = (sequelize, DataTypes) => {
  const Swapkit = sequelize.define('Swapkit', {
    kitName: DataTypes.STRING,
    orderNo: DataTypes.STRING,
    shipTo: DataTypes.STRING,
    orderStatus: {
      type: DataTypes.ENUM,
      values: ['ordered', 'canceled']
    },
    orderedBy: {
      type: DataTypes.ENUM,
      values: ['employee', 'contractor']
    },
    orderCopy: DataTypes.TEXT('long'),
    userId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      }
    },
    agentId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Users',
        key: 'id'
      }
    },
    orderDate: DataTypes.DATE,
    cancelDate: DataTypes.DATE,
    patientId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'Patients',
        key: 'id'
      }
    },
    DeletedAt: DataTypes.DATE
  }, {});
  Swapkit.associate = function(models) {
    // associations can be defined here
    // Swapkit.belongsTo(models.Users, {
    //   foreignKey: 'userId',
    //   targetKey: 'id'
    // });
    // Swapkit.belongsTo(models.Patient, {
    //   foreignKey: 'patiantId',
    //   targetKey: 'id'
    // });
    Swapkit.belongsTo(models.Patient, {
      as: 'patient',
      foreignKey: 'patientId',
      targetKey: 'id'
    });

    Swapkit.belongsTo(models.Users, {
      as: 'agent',
      foreignKey: 'agentId',
      targetKey: 'id'
    });
  };
  return Swapkit;
};