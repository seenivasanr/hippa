'use strict';
module.exports = (sequelize, DataTypes) => {
  const Forms = sequelize.define('Forms', {
    testName: DataTypes.STRING,
    formFields: DataTypes.TEXT('long'),
    createType: {
      type: DataTypes.ENUM,
      values: ['custom', 'default']
    },
    DeletedAt: DataTypes.DATE
  }, {});
  Forms.associate = function(models) {
    // associations can be defined here
  };
  return Forms;
};