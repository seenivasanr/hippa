'use strict';
const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
    fullName: DataTypes.STRING,
    pharmacyName: DataTypes.STRING,
    constractorId: {
      type: DataTypes.STRING,
      unique: true
    },
    employeeNo: {
      type: DataTypes.STRING,
      unique: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please enter email'
        }
      }
    },
    pharmacyNpi: DataTypes.STRING,
    businessEin: DataTypes.STRING,
    streetAddress: DataTypes.STRING,
    phoneNo: DataTypes.STRING,
    userName: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please Enter User Name'
        }
      }
    },
    FaxNo: DataTypes.STRING,
    state: DataTypes.STRING,
    city: DataTypes.STRING,
    zipcode: DataTypes.STRING,
    ncpa: DataTypes.STRING,
    partnerName: DataTypes.STRING,
    partnerId: DataTypes.STRING,
    image_url: DataTypes.STRING,
    dob: DataTypes.DATE,
    password: DataTypes.STRING,
    gender: {
      type: DataTypes.ENUM,
      values: ['male', 'female', 'others']
    },
    role: {
      type: DataTypes.ENUM,
      values: ['admin','contractor','employee', 'physician']
    },
    position: {
      type: DataTypes.ENUM,
      values: ['interviewer','viewer','agent']
    },
    permission: {
      type:DataTypes.ENUM,
      values:['canview','canedit']
    },
    status: DataTypes.BOOLEAN,
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    IsDelete: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    DeletedAt: DataTypes.DATE,

    healthSystem: DataTypes.STRING,
    groupPracticeName: DataTypes.STRING,
    licenseNo: DataTypes.STRING,
    npiNo: DataTypes.STRING,
    einNo: DataTypes.STRING,
    primaryNo: DataTypes.STRING,
    // name: {
    //   type: DataTypes.STRING,
    //   unique: true
    // },
    // email: {
    //   type: DataTypes.STRING,
    //   unique: true,
    //   allowNull: false,
    //   validate: {
    //     notNull: {
    //       msg: 'Please enter email'
    //     }
    //   }
    // },
    // phoneNo: DataTypes.STRING,
    // FaxNo: DataTypes.STRING,
    // address: DataTypes.STRING,
    // city: DataTypes.STRING,
    // state: DataTypes.STRING,
    // zipcode: DataTypes.STRING,
  }, {
    
  });
  Users.associate = function(models) {
    // associations can be defined here
    // Users.associate = function (models) {
      // Users.hasMany(models.Patient, {
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
      
      // Users.hasMany(models.Swapkits, {
      //   foreignKey: 'userId',
      //   sourceKey: 'id'
      // });
    // };
  };
  Users.prototype.validPassword = function(password) {
      return bcrypt.compareSync(password, this.password);
  };
  return Users;
};