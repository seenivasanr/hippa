'use strict';
module.exports = (sequelize, DataTypes) => {
  const Labs = sequelize.define('Labs', {
    labName: DataTypes.STRING,
    nameOfContact: DataTypes.STRING,
    offeredService: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      unique: false,
      allowNull: false,
      validate: {
        notNull: {
          msg: 'Please enter email'
        }
      }
    },
    address: DataTypes.STRING,
    phoneNo: DataTypes.STRING,
    FaxNo: DataTypes.STRING,
    city: DataTypes.STRING,
    state: DataTypes.STRING,
    zipcode: DataTypes.STRING,
    clinNo: DataTypes.STRING,
    DeletedAt: DataTypes.DATE
  }, {});
  Labs.associate = function(models) {
    // associations can be defined here
    // Labs.associate = function (models) {
      // Labs.hasMany(models.Patient, {
      //   foreignKey: 'labId',
      //   sourceKey: 'id'
      // });
    // };
  };
  return Labs;
};