var styles = require("../common/styles");
var reportData = {};
// Dynamic Fields
reportData.sheet_name = 'Contractor Reports';
reportData.file_name = 'Contractor_Reports.xlsx';

reportData.heading = [											// Headings
    [
        {value: reportData.sheet_name, style: styles.headerDark}, 	// Sheet Name for Heading
        {value: 'b1', style: styles.headerDark}, 
        {value: 'c1', style: styles.headerDark}
    ],
];
reportData.specification = {									// Coloumn Header
    fullName: { 
        displayName: 'FUll Name', 
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    constractorId: {
        displayName: 'Contractor ID',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    phoneNo: {
        displayName: 'Phone Number',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    email: {
        displayName: 'Email Address',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    streetAddress: {
        displayName: 'Address',
        headerStyle: styles.coloumnHeader,
        width: 200
    }
}

reportData.merges = [											// Merges
    { 
        start: { row: 1, column: 1 }, 
        end: { row: 1, column: Object.keys(reportData.specification).length } 
    },
]

module.exports = reportData;