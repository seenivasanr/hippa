var styles = require("../common/styles");
var reportData = {};

// Dynamic Fields
reportData.sheet_name = 'Insurance Reports';
reportData.file_name = 'Insurance_Reports.xlsx';

reportData.heading = [	// Headings
    [
        {value: reportData.sheet_name, style: styles.headerDark}, 	// Sheet Name for Heading
        {value: 'b1', style: styles.headerDark}, 
        {value: 'c1', style: styles.headerDark}
    ],
];
reportData.specification = {									// Coloumn Header
    'lab.labName': { 
        displayName: 'Lab Name', 
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    preInsuranceType: {
        displayName: 'Insurance Type',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    fullName: {
        displayName: 'Patient Name',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    dob: {
        displayName: 'Patient DOB',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    testStatus: {
        displayName: 'Status',
        headerStyle: styles.coloumnHeader,
        width: 200
    },
    statusBasedDayDiff: {
        displayName: 'Days in Status',
        headerStyle: styles.coloumnHeader,
        width: 200
    },
    submissionDate: {
        displayName: 'Submission Date',
        headerStyle: styles.coloumnHeader,
        width: 200
    },
    resultedDayDiff: {
        displayName: 'TOTAL DAYS',
        headerStyle: styles.coloumnHeader,
        width: 300
    }
}

reportData.merges = [											// Merges
    { 
        start: { row: 1, column: 1 }, 
        end: { row: 1, column: Object.keys(reportData.specification).length } 
    },
]

module.exports = reportData;