var styles = require("../common/styles");
var reportData = {};

// Dynamic Fields
reportData.sheet_name = 'Lab Reports';
reportData.file_name = 'Lab_Reports.xlsx';

reportData.heading = [	// Headings
    [
        {value: reportData.sheet_name, style: styles.headerDark}, 	// Sheet Name for Heading
        {value: 'b1', style: styles.headerDark}, 
        {value: 'c1', style: styles.headerDark}
    ],
];
reportData.specification = {									// Coloumn Header
    labName: { 
        displayName: 'Laboratory Name', 
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    nameOfContact: {
        displayName: 'Name of Contact',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    offeredService: {
        displayName: 'Services',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    phoneNo: {
        displayName: 'Phone Number',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    email: {
        displayName: 'Email Address',
        headerStyle: styles.coloumnHeader,
        width: 200
    }
}

reportData.merges = [											// Merges
    { 
        start: { row: 1, column: 1 }, 
        end: { row: 1, column: Object.keys(reportData.specification).length } 
    },
]

module.exports = reportData;