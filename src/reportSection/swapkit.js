var styles = require("../common/styles");
var reportData = {};

// Dynamic Fields
reportData.sheet_name = 'Swap Kits Reports';
reportData.file_name = 'SwapKit_Reports.xlsx';

reportData.heading = [	// Headings
    [
        {value: reportData.sheet_name, style: styles.headerDark}, 	// Sheet Name for Heading
        {value: 'b1', style: styles.headerDark}, 
        {value: 'c1', style: styles.headerDark}
    ],
];
reportData.specification = {									// Coloumn Header
    orderNo: { 
        displayName: 'Order Number', 
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    ['patient.firstName']: {
        displayName: 'First Name',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    ['patient.lastName']: {
        displayName: 'Last Name',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    ['patient.gender']: {
        displayName: 'Gender',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    orderStatus: {
        displayName: 'Order Status',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    orderDate: {
        displayName: 'Ordered Date',
        headerStyle: styles.coloumnHeader,
        width: 200
    }
}

reportData.merges = [											// Merges
    { 
        start: { row: 1, column: 1 }, 
        end: { row: 1, column: Object.keys(reportData.specification).length } 
    },
]

module.exports = reportData;