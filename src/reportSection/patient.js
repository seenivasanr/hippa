var styles = require("../common/styles");
var reportData = {};

// Dynamic Fields
reportData.sheet_name = 'Patient Reports';
reportData.file_name = 'Patient_Reports.xlsx';

reportData.heading = [ // Headings
    [
        {value: reportData.sheet_name, style: styles.headerDark}, // Sheet Name for Heading
        {value: 'b1', style: styles.headerDark},
        {value: 'c1', style: styles.headerDark}
    ],
];
reportData.specification = { // Coloumn Header
    firstName: {
        displayName: 'Patient Name',
        headerStyle: styles.coloumnHeader,
        width: 120,
        cellFormat: function(value, row) {
            return row.firstName + ' ' + row.lastName;
          },
    },
    email: {
        displayName: 'Email',
        headerStyle: styles.coloumnHeader,
        width: 140
    },
    gender: {
        displayName: 'Gender',
        headerStyle: styles.coloumnHeader,
        width: 120
    },
    dob: {
        displayName: 'DOB',
        headerStyle: styles.coloumnHeader,
        width: 120
    },
    phoneNo: {
        displayName: 'Phone Number',
        headerStyle: styles.coloumnHeader,
        width: 200
    },
    testStatus: {
        displayName: 'Status',
        headerStyle: styles.coloumnHeader,
        width: 200
    }
}

reportData.merges = [ // Merges
    {
        start: { row: 1, column: 1 },
        end: { row: 1, column: Object.keys(reportData.specification).length }
    },
]

module.exports = reportData;