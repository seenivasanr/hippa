var styles = require("../common/styles");
var reportData = {};

// Dynamic Fields
reportData.sheet_name = 'Physician Reports';
reportData.file_name = 'Physician_Reports.xlsx';

reportData.heading = [ // Headings
    [
        {value: reportData.sheet_name, style: styles.headerDark}, // Sheet Name for Heading
        {value: 'b1', style: styles.headerDark},
        {value: 'c1', style: styles.headerDark}
    ],
];
reportData.specification = { // Coloumn Header
    name: {
        displayName: 'Physician Name',
        headerStyle: styles.coloumnHeader,
        width: 120
    },
    groupPracticeName: {
        displayName: 'Group Practice Name',
        headerStyle: styles.coloumnHeader,
        width: 180
    },
    npiNo: {
        displayName: 'NPI',
        headerStyle: styles.coloumnHeader,
        width: 120
    },
    licenseNo: {
        displayName: 'License',
        headerStyle: styles.coloumnHeader,
        width: 120
    },
    phoneNo: {
        displayName: 'Phone Number',
        headerStyle: styles.coloumnHeader,
        width: 200
    }
}

reportData.merges = [ // Merges
    {
        start: { row: 1, column: 1 },
        end: { row: 1, column: Object.keys(reportData.specification).length }
    },
]

module.exports = reportData;