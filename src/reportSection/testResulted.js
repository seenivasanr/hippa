var styles = require("../common/styles");
var reportData = {};

// Dynamic Fields
reportData.sheet_name = 'Test Resulted Reports';
reportData.file_name = 'Test_Resulted_Reports.xlsx';

reportData.heading = [	// Headings
    [
        {value: reportData.sheet_name, style: styles.headerDark}, 	// Sheet Name for Heading
        {value: 'b1', style: styles.headerDark}, 
        {value: 'c1', style: styles.headerDark}
    ],
];
reportData.specification = {									// Coloumn Header
    'agent.partnerName': { 
        displayName: 'Partner ID', 
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    'agent.fullName': {
        displayName: 'Pharmacy Name',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    fullName: {
        displayName: 'Patient Name',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    dob: {
        displayName: 'Patient DOB',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    phoneNo: {
        displayName: 'Patient Phone',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    resulted: {
        displayName: 'Resulted Date',
        headerStyle: styles.coloumnHeader,
        width: 200
    },
    'physician.name': {
        displayName: 'Doctor Name',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    'physician.phoneNo': {
        displayName: 'Doctor Phone',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    'physician.FaxNo': {
        displayName: 'Doctor Fax',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
}

reportData.merges = [											// Merges
    { 
        start: { row: 1, column: 1 }, 
        end: { row: 1, column: Object.keys(reportData.specification).length } 
    },
]

module.exports = reportData;