var styles = require("../common/styles");
var reportData = {};

// Dynamic Fields
reportData.sheet_name = 'New Submissions Reports';
reportData.file_name = 'New_Submissions_Reports.xlsx';

reportData.heading = [	// Headings
    [
        {value: reportData.sheet_name, style: styles.headerDark}, 	// Sheet Name for Heading
        {value: 'b1', style: styles.headerDark}, 
        {value: 'c1', style: styles.headerDark}
    ],
];
reportData.specification = {									// Coloumn Header
    partnerId: { 
        displayName: 'Partner ID', 
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    fullName: {
        displayName: 'Pharmacy Contractor Name',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    state: {
        displayName: 'Contractor State',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    thisMonth: {
        displayName: '# of New Submissions',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    lastMonth: {
        displayName: 'Submissions Last Month',
        headerStyle: styles.coloumnHeader,
        width: 200
    },
    thisYear: {
        displayName: 'Submissions YTD',
        headerStyle: styles.coloumnHeader,
        width: 200
    }
}

reportData.merges = [											// Merges
    { 
        start: { row: 1, column: 1 }, 
        end: { row: 1, column: Object.keys(reportData.specification).length } 
    },
]

module.exports = reportData;