var styles = require("../common/styles");
var reportData = {};

// Dynamic Fields
reportData.sheet_name = 'Payroll Reports';
reportData.file_name = 'Payroll_Reports.xlsx';

reportData.heading = [	// Headings
    [
        {value: reportData.sheet_name, style: styles.headerDark}, 	// Sheet Name for Heading
        {value: 'b1', style: styles.headerDark}, 
        {value: 'c1', style: styles.headerDark}
    ],
];
reportData.specification = {									// Coloumn Header
    'agent.partnerId': { 
        displayName: 'Partner ID', 
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    'agent.fullName': {
        displayName: 'Pharmacy Name',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    fullName: {
        displayName: 'Patient Name',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    dob: {
        displayName: 'Patient DOB',
        headerStyle: styles.coloumnHeader,
        width: 120 
    },
    resulted: {
        displayName: 'Resulted Date',
        headerStyle: styles.coloumnHeader,
        width: 200
    }
}

reportData.merges = [											// Merges
    { 
        start: { row: 1, column: 1 }, 
        end: { row: 1, column: Object.keys(reportData.specification).length } 
    },
]

module.exports = reportData;